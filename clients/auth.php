<?php
	/**
	 * Created by PhpStorm.
	 * User: jake
	 * Date: 06/06/2018
	 * Time: 15:53
	 */

	//include_once ('database.php');

	class auth {


		private $database;

		/**
		 * auth constructor.
		 *
		 * @param $db database
		 */
		public function __construct( $db ) {
			$this->database = $db;
		}

		public function checkAuth() {
			if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == 1 && isset($_SESSION['userid']) && isset($_SESSION['usertoken'])) {
				$token = $this->getToken($_SESSION['userid']);

				if($token != $_SESSION['usertoken']){
					$_SESSION['loggedin'] = 0;
					header('Location: login.php');
				}
			} else {
				$_SESSION['loggedin'] = 0;
				header('Location: login.php');
			}
		}

		public function getToken( $userID ) {
			$response = $this->database->query('SELECT * FROM users WHERE id=' . $userID);

			return $response[0]['token'];
		}

		public function getCurrentUser() {
			return $this->getUser($_SESSION['userid']);
		}

		public function getUser( $userID ) {
			$response = $this->database->query('SELECT * FROM users WHERE id=' . $userID);

			return $response[0];
		}

		public function loggedin() {
			if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == 1) {
				return true;
			} else {
				return false;
			}
		}

	}