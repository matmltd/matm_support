<?php
	/**
	 * Created by PhpStorm.
	 * User: jake
	 * Date: 07/06/2018
	 * Time: 10:03
	 */


	if(!isset($_POST)){
		header('Location: index.php');
	}

	if(!isset($_GET)){
		header('Location: index.php');
	}
	session_start();

	ini_set('display_errors', 1);
	date_default_timezone_set('Etc/UTC');

	require_once ('webfaction.php');
	require_once ('functions.php');

	auth();

	$webfaction = new \Webfaction\webfaction('matm', 'w$@q62y-6kiap$uf', $auth);

	$webfaction->get_all_emailAddresses();

	if($_GET['action'] == "updatePassword") {

		if($_POST['password'] !== $_POST['passwordSecond']) {
			header( 'Location: editMailbox.php?m=Error&id=' . $_POST['mailbox'] );
		} else {

			$response = $webfaction->updateMailboxPassword( $_POST['mailbox'], $_POST['password'] );

			if ( $response === false ) {
				header( 'Location: editMailbox.php?m=Error&id=' . $_POST['mailbox'] );
			} else {
				header( 'Location: editMailbox.php?m=Success&id=' . $_POST['mailbox'] );
			}
		}
	} elseif ($_GET['action'] == "create"){
		$response = $webfaction->createEmailAccount();
	}