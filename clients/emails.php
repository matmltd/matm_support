<?php
	/**
	 * Created by PhpStorm.
	 * User: jake
	 * Date: 04/06/2018
	 * Time: 16:06
	 */

	session_start();

	ini_set('display_errors', 1);
	date_default_timezone_set('Etc/UTC');

	require_once ('webfaction.php');
	require_once ('functions.php');

	auth();

	$webfaction = new \Webfaction\webfaction('matm', 'w$@q62y-6kiap$uf', $auth);

	$emailAdresses = $webfaction->getEmailAddresses();

	printHeader();
?>


<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Email Accounts</div>

				<div class="panel-body">
					<?php
                        $count=1;
					foreach($emailAdresses as $email_address){
					    $emailAccount = $webfaction->get_email_account_from_address($email_address);
					?>
						<div class="account">
							<div class="acount_inner">
								<h3><?=$email_address?></h3>
                                <?php
                                    if($emailAccount['autoresponder_subject'] == "" || $emailAccount['autoresponder_message'] == "" || $emailAccount['autoresponder_from'] == "") {
                                        ?>
                                        <div class="buttons right" title="Please set Auto-Responder subject, message and from email before enabling" data-toggle="tooltip" data-placement="top">
                                            <input type="checkbox" name="checkbox<?=$count?>" id="checkbox<?=$count?>"
                                                   class="ios-toggle"
			                                    disabled data-email="<?= $email_address ?>" />
                                            <label for="checkbox<?=$count?>" class="checkbox-label" data-off="off"
                                                   data-on="on"></label>
                                            <p><a href="editAccount.php?id=<?= urlencode( $email_address ) ?>">Edit</a></p>
                                        </div>
                                        <div style="clear: both;"></div>

	                                    <?php
                                    } else {
	                                    if ( $emailAccount != false ) {
		                                    if ( $emailAccount['autoresponder_on'] ) {
			                                    $checked = "checked";
		                                    } else {
			                                    $checked = "";
		                                    }
		                                    ?>
                                            <div class="buttons right">
                                                <input type="checkbox" name="checkbox<?=$count?>" id="checkbox<?=$count?>"
                                                       class="ios-toggle"
				                                    <?= $checked ?> data-email="<?= $email_address ?>"/>
                                                <label for="checkbox<?=$count?>" class="checkbox-label" data-off="off"
                                                       data-on="on"></label>
                                                <p><a href="editAccount.php?id=<?= urlencode( $email_address ) ?>">Edit</a></p>

                                            </div>
                                            <div style="clear: both;"></div>

		                                    <?php
	                                    }
                                    }
                                    ?>
							</div>
						</div>
					<?php
                        $count++;
					}
					?>
				</div>
			</div>
		</div>
	</div>
</div>

    <script>
        $(document).ready(function () {
            $('.ios-toggle').change(function () {
                $(this).attr("disabled", true);
                $(this).parent().parent().append('<div class="updating alert alert-info"><p>Updating, please wait</p></div>');
                var email = $(this).data('email');
                var theElememt = this;
                $.get( "updateAccount.php", { email: email, action: 'enableForwarder', checked: this.checked} ).done(function( data  ) {
                    var parent = $(theElememt).parent().parent(),
                    box = $(parent).find('.updating');
                    console.log(theElememt);
                    if(data == "1"){
                        $(theElememt).removeAttr("disabled");
                        $(box).removeClass('alert-info');
                        $(box).addClass('alert-success');
                        $(box).html("<p>Success, your auto response has been updated</p>");
                        setTimeout(function() {removeBox(box)}, 5000);
                        console.log(data);
                    } else {
                        $(box).html("<p>Oops. There was a problem, please try again later</p>");
                        $(theElememt).prop("checked", !$(theElememt).prop("checked"));
                        $(box).removeClass('alert-info');
                        $(box).addClass('alert-danger');
                        $(theElememt).removeAttr("disabled");
                        setTimeout(function() {removeBox(box)}, 5000);
                        console.log(data);
                    }
                });
            });
        });

        function removeBox(box){
            $(box).animate({
                height: "0px",
            }, 400, function() {$(box).remove()});
        }
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>

<?php
    printFooter();