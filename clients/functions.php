<?php
	/**
	 * Created by PhpStorm.
	 * User: jake
	 * Date: 06/06/2018
	 * Time: 15:38
	 */

	require_once ('database.php');
	require_once ('auth.php');

	function init() {
		global $db;
		if($_SERVER['HTTP_HOST'] == "localhost"){
			$db = new database('localhost', 'root', 'root', 'website_manager');
		} elseif ($_SERVER['HTTP_HOST'] == "support.matm.co.uk") {
			$db = new database('localhost', 'matm_support', 'studio32', 'matm_clients');
		} else {
			$db = null;
		}
		return $db;
	}

	$db = init();

	$auth = new auth($db);

	function auth(){
		global $auth;

		$auth->checkAuth();

	}

	function printHeader() {
		global $auth;
		$auth->checkAuth();
		$html = "<!DOCTYPE html>
<html lang=\"en\">
<head>
	<meta name=\"csrf-token\" content=\"{{ csrf_token() }}\" />
	<meta charset=\"utf-8\">
	<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
	<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

	<title>MATM Website Manager</title>

	<!-- Fonts -->
	<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css\" integrity=\"sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+\" crossorigin=\"anonymous\">
	<link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Lato:100,300,400,700\">

	<!-- Styles -->
	<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css\" integrity=\"sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7\" crossorigin=\"anonymous\">
	<link href=\"style.css\" rel=\"stylesheet\">
	<style>
		body {
			font-family: 'Lato';
		}

		.fa-btn {
			margin-right: 6px;
		}
		a.navbar-brand {
			padding: 0;
		}
	</style>
	<script
		src=\"https://code.jquery.com/jquery-3.2.1.js\"
		integrity=\"sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=\"
		crossorigin=\"anonymous\"></script>


</head>
<body id=\"app-layout\">
<nav class=\"navbar navbar-default navbar-static-top\">
	<div class=\"container\">
		<div class=\"navbar-header\">

			<!-- Collapsed Hamburger -->
			<button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#app-navbar-collapse\">
				<span class=\"sr-only\">Toggle Navigation</span>
				<span class=\"icon-bar\"></span>
				<span class=\"icon-bar\"></span>
				<span class=\"icon-bar\"></span>
			</button>

			<!-- Branding Image -->
			<a class=\"navbar-brand\" href=\"\">
				<?xml version=\"1.0\" encoding=\"utf-8\"?>
<!-- Generator: Adobe Illustrator 18.1.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">
<svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"
	 width=\"200px\" height=\"50px\"  viewBox=\"-235 409.9 124 23\" enable-background=\"new -235 409.9 124 23\" xml:space=\"preserve\">
<g>

	<path fill=\"#FCC200\" d=\"M-189.8,430.7c-0.3,0.1-0.6,0.1-0.9,0.1c-0.6,0-1.2-0.2-1.6-0.7c-0.4-0.4-0.7-1-0.9-1.6
		c-0.2-0.7-0.4-1.3-0.5-2c-0.1-0.7-0.2-1.3-0.3-1.7c-0.6,1.3-1.5,2.5-2.6,3.6c-1.1,1.2-2.2,2.1-3.5,2.8c-0.2-0.5-0.3-1-0.3-1.5
		c0-0.5,0.1-1,0.3-1.6c0.2-0.6,0.4-1.1,0.5-1.7c0.2-0.6,0.3-1.1,0.3-1.7c0-0.4-0.1-0.9-0.2-1.3c-0.5,0.3-1,0.7-1.5,1.2
		c-0.5,0.4-0.9,0.9-1.4,1.3c-0.3,0.3-0.6,0.7-1.1,1.2c-0.5,0.5-1,1-1.5,1.6c-0.6,0.6-1.1,1-1.5,1.5c-0.5,0.4-0.9,0.7-1.1,0.8
		c0-0.2,0-0.4,0-0.5c0-0.5,0.2-1.1,0.5-1.6c0.3-0.5,0.7-1,1.1-1.5c0.4-0.5,0.7-1,0.9-1.5c0.2-0.5,0.4-1.1,0.7-1.8
		c0.3-0.7,0.6-1.4,1-2c0.4-0.6,0.8-1.1,1.3-1.3c0,0.5-0.1,1-0.3,1.5c-0.2,0.5-0.4,1-0.5,1.6c-0.2,0.5-0.3,1-0.3,1.5
		c0.3-0.3,0.7-0.6,1.2-1.1c0.5-0.5,1-0.9,1.5-1.3c0.5-0.4,1-0.6,1.3-0.6c0.5,0,0.8,0.3,1,0.7c0.2,0.4,0.3,0.9,0.3,1.3
		c0,0.6-0.1,1.2-0.3,1.8c-0.1,0.2-0.2,0.5-0.3,0.9c-0.1,0.4-0.2,0.7-0.3,1.1c-0.1,0.4-0.1,0.7-0.1,0.9c0.5-0.3,1-0.7,1.4-1.2
		c0.4-0.5,0.8-0.9,1.2-1.5c0.7-1,1.4-2.1,2.1-3.1c0.2-0.3,0.5-0.7,0.8-1c0.3-0.4,0.7-0.6,1.1-0.7c0,0.3,0,0.7,0.1,1.1
		c0,0.7-0.1,1.4-0.2,2c-0.1,0.7-0.1,1.4-0.2,2c0,1,0.2,1.9,0.6,2.6C-191.5,429.9-190.8,430.4-189.8,430.7L-189.8,430.7z\"/>
	<path fill-rule=\"evenodd\" clip-rule=\"evenodd\" fill=\"#FCC200\" d=\"M-169,427.8c-1.2,1.2-2.5,2-3.8,2.5c-1.3,0.5-2.8,0.7-4.5,0.7
		c-0.8,0-1.7-0.1-2.5-0.3c-0.8-0.2-1.4-0.7-1.9-1.5c-0.8,0.5-1.6,1-2.5,1.4c-0.8,0.4-1.7,0.7-2.7,0.7c-1,0-1.5-0.5-1.5-1.4
		c0-0.7,0.2-1.4,0.6-2.1c0.4-0.7,0.9-1.3,1.4-1.9c0.6-0.6,1.1-1.1,1.6-1.5c0.6-0.5,1.2-0.9,1.9-1.4c0.7-0.5,1.4-0.8,2.1-1.2
		c0.7-0.3,1.5-0.5,2.3-0.5c0.7,0,1.3,0.3,1.6,0.8c0.3,0.5,0.5,1.1,0.5,1.8c0,0.7-0.2,1.4-0.5,2.1c-0.3,0.6-0.8,1.2-1.3,1.7
		c-0.5,0.5-1,1-1.5,1.5c0.5,0.3,1.1,0.5,1.6,0.7c0.6,0.1,1.2,0.2,1.8,0.2c1.2,0,2.4-0.3,3.5-0.7c1.1-0.4,2.1-0.9,3.2-1.5l0.2-0.2
		L-169,427.8L-169,427.8z M-177.2,423.9c0-0.5-0.1-0.9-0.3-1.3c-0.2-0.4-0.6-0.5-1.1-0.6c-1,0-1.9,0.3-2.9,0.9
		c-0.9,0.5-1.8,1.1-2.5,1.7c-0.5,0.4-0.9,0.8-1.4,1.3c-0.5,0.5-0.9,1-1.2,1.6c-0.3,0.6-0.5,1.2-0.5,1.8c0,0.3,0.1,0.5,0.2,0.7
		c0.1,0.2,0.3,0.3,0.6,0.3c0.4,0,0.8-0.1,1.3-0.4c0.5-0.2,1-0.5,1.4-0.8c0.5-0.3,0.8-0.6,1.1-0.9c0.3-0.3,0.6-0.7,0.8-1
		c0.2-0.4,0.4-0.7,0.6-1.1c0.1-0.2,0.3-0.3,0.5-0.3c0.2,0,0.4-0.1,0.7-0.1c0,0.3-0.1,0.6-0.1,0.9c0,0.3,0,0.6,0.1,1
		c0.7-0.4,1.3-0.9,1.9-1.6C-177.5,425.5-177.2,424.8-177.2,423.9L-177.2,423.9z\"/>
	<path fill=\"#FCC200\" d=\"M-152.2,417.5c-0.5,0.6-1.1,1.1-1.9,1.5c-0.7,0.4-1.5,0.7-2.2,0.9c-1.1,0.3-2.1,0.6-3.2,1
		c-0.2,0.1-0.5,0.2-0.8,0.2c-0.3,0.1-0.5,0.2-0.7,0.3c-0.4,0.3-0.8,0.7-1.1,1.3c-0.3,0.6-0.6,1.1-0.8,1.6c-0.6,1.5-1.3,3-1.9,4.6
		c-0.1,0.3-0.2,0.6-0.4,1c-0.2,0.4-0.3,0.8-0.5,1c-0.1-0.4-0.1-0.8-0.1-1.2c0-0.8,0.2-1.7,0.6-2.6c0.4-0.9,0.8-1.8,1.2-2.6
		s0.8-1.7,1.1-2.5c-1-0.1-1.9-0.2-2.9-0.3c-0.5-0.1-0.9-0.2-1.3-0.5c-0.4-0.3-0.7-0.7-0.8-1.1c0.2,0,0.3,0,0.5,0
		c0.6,0,1.2,0.1,1.8,0.3c0.6,0.2,1.2,0.3,1.8,0.3c0.7,0,1.3-0.3,1.7-0.7c0.4-0.5,0.7-1,0.9-1.6c0.1-0.2,0.2-0.5,0.4-1
		c0.2-0.4,0.4-0.9,0.6-1.3c0.2-0.4,0.4-0.7,0.6-0.8c0,0.2,0,0.4,0,0.6c0,0.7-0.1,1.3-0.3,2c-0.2,0.7-0.3,1.3-0.3,2c0,0,0,0.1,0,0.1
		c0,0.1,0,0.1,0,0.1c1.3-0.4,2.7-0.8,4-1.3C-154.9,418.4-153.6,417.9-152.2,417.5L-152.2,417.5z\"/>
	<path fill=\"#FCC200\" d=\"M-137.2,430.7c-0.3,0.1-0.6,0.1-0.9,0.1c-0.6,0-1.2-0.2-1.6-0.7c-0.4-0.4-0.7-1-0.9-1.6
		c-0.2-0.7-0.4-1.3-0.5-2c-0.1-0.7-0.2-1.3-0.3-1.7c-0.6,1.3-1.5,2.5-2.6,3.6c-1.1,1.2-2.2,2.1-3.5,2.8c-0.2-0.5-0.3-1-0.3-1.5
		c0-0.5,0.1-1,0.3-1.6c0.2-0.6,0.4-1.1,0.5-1.7c0.2-0.6,0.3-1.1,0.3-1.7c0-0.4-0.1-0.9-0.2-1.3c-0.5,0.3-1,0.7-1.5,1.2
		c-0.5,0.4-0.9,0.9-1.4,1.3c-0.3,0.3-0.6,0.7-1.1,1.2c-0.5,0.5-1,1-1.5,1.6c-0.6,0.6-1.1,1-1.5,1.5s-0.9,0.7-1.1,0.8
		c0-0.2,0-0.4,0-0.5c0-0.5,0.2-1.1,0.5-1.6c0.3-0.5,0.7-1,1.1-1.5c0.4-0.5,0.7-1,0.9-1.5c0.2-0.5,0.4-1.1,0.7-1.8
		c0.3-0.7,0.6-1.4,1-2c0.4-0.6,0.8-1.1,1.3-1.3c0,0.5-0.1,1-0.3,1.5c-0.2,0.5-0.4,1-0.5,1.6c-0.2,0.5-0.3,1-0.3,1.5
		c0.3-0.3,0.7-0.6,1.2-1.1c0.5-0.5,1-0.9,1.5-1.3c0.5-0.4,1-0.6,1.3-0.6c0.5,0,0.8,0.3,1,0.7c0.2,0.4,0.3,0.9,0.3,1.3
		c0,0.6-0.1,1.2-0.3,1.8c-0.1,0.2-0.1,0.5-0.3,0.9c-0.1,0.4-0.2,0.7-0.3,1.1c-0.1,0.4-0.1,0.7-0.1,0.9c0.5-0.3,1-0.7,1.4-1.2
		c0.4-0.5,0.8-0.9,1.2-1.5c0.7-1,1.4-2.1,2.1-3.1c0.2-0.3,0.5-0.7,0.8-1c0.3-0.4,0.7-0.6,1.1-0.7c0,0.3,0,0.7,0.1,1.1
		c0,0.7-0.1,1.4-0.2,2c-0.1,0.7-0.1,1.4-0.2,2c0,1,0.2,1.9,0.6,2.6C-138.9,429.9-138.2,430.4-137.2,430.7L-137.2,430.7z\"/>
</g>
</svg>

			</a>
		</div>

		<div class=\"collapse navbar-collapse\" id=\"app-navbar-collapse\">
			<!-- Left Side Of Navbar -->
			<ul class=\"nav navbar-nav\">
				<li><a href=\"index.php\">Home</a></li>
				<li>
					<a href=\"emails.php\">Auto Replies</a>
					<!--<ul>
						<li>
							<a href=\"emails.php\">View Email Acounts</a>
						</li>
						<li>
							<a href=\"addemail.php\">Add Email Account</a>
						</li>
					</ul>-->
				</li>
				<li>
					<a href=\"mailbox.php\">Email Accounts</a>
					<!--<ul>
						<li>
							<a href=\"mailbox.php\">View Mailboxes</a>
						</li>
						<li>
							<a href=\"addmailbox.php\">Add Mailbox</a>
						</li>
					</ul>-->
				</li>
				<li>
					<a href=\"websites.php\">Websites</a>
				</li>
			</ul>

			<!-- Right Side Of Navbar -->
			<ul class=\"nav navbar-nav navbar-right\">
				<!-- Authentication Links -->";
					if(!$auth->loggedin()) {
						$html .= "<li><a href=\"login.php\">Login</a></li>";
					} else {
						$html .= "<li class=\"dropdown\">
							<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">" . $auth->getCurrentUser()['username'] . "<span class=\"caret\"></span>
							</a>

							<ul class=\"dropdown-menu\" role=\"menu\">
								<li><a href=\"logout.php\"><i class=\"fa fa-btn fa-sign-out\"></i>Logout</a></li>
							</ul>
						</li>";
					}
					$html .= "
			</ul>
		</div>
	</div>
</nav>";
					print $html;
	}

	function printFooter() {
		print "
<script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js\" integrity=\"sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb\" crossorigin=\"anonymous\"></script>
<script src=\"https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js\" integrity=\"sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS\" crossorigin=\"anonymous\"></script>
</body>
</html>";
	}