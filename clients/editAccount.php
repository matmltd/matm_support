<?php
	/**
	 * Created by PhpStorm.
	 * User: jake
	 * Date: 04/06/2018
	 * Time: 16:06
	 */

	session_start();

	ini_set('display_errors', 1);
	date_default_timezone_set('Etc/UTC');

	require_once ('webfaction.php');
	require_once ('functions.php');

	auth();

	$webfaction = new \Webfaction\webfaction('matm', 'w$@q62y-6kiap$uf', $auth);
	if(!isset($_GET['id'])){
		print "Error: Email Address not found. Please go back and try again";
		exit;
	}
	$emailAdress = urldecode($_GET['id']);
	$emailAdresses = $webfaction->getAllEmailAccounts();
    if(!isset($emailAdresses[urlencode($emailAdress)])){
	    print "Error: Email Address not found. Please go back and try again</br>";
	    print "<a href='index.php'>Click here to go back Home</a>";
	    exit;
    }
	$emailAccount = $emailAdresses[urlencode($emailAdress)];

    printHeader();
?>

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Edit Email Account - <?=$emailAdress?></div>
				<div class="panel-body">
					<?php
						if(isset($_GET['m'])){
							if($_GET['m'] == "Success"){
								?>
                                <div class="alert alert-success">
                                    <p><strong>Success!</strong> Your Account has been updated</p>
                                </div>
								<?php
							} elseif ($_GET['m'] == "Error") {
								?>
                                <div class="alert alert-danger">
                                    <p><strong>Error!</strong> Your Account has not updated. Please try again later</p>
                                </div>
								<?php
							}
						}
					?>
                    <form action="updateAccount.php?action=update" method="POST">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" disabled class="form-control" id="emailAddressDummy" name="emailAddressDummy" aria-describedby="emailHelp" placeholder="Email Address" value="<?=$emailAdress?>"/>
                            <input type="hidden"  class="form-control" id="emailAddress" name="emailAddress" placeholder="Email Address" value="<?=$emailAdress?>"/>
                        </div>
                        <div class="form-group">
                            <!--<label for="mailbox">Mailbox / Forwarder</label>-->
                            <input type="hidden" class="form-control" id="mailbox" name="mailbox" placeholder="Mailbox" value="<?=$emailAccount['targets']?>"/>
                            <!--<p>Please Enter the name of the Mailbox you wish to save incoming emails too, or an email address to forward emails too</p>-->
                        </div>
                        <div class="form-check">
                            <?php
                            if($emailAccount['autoresponder_on']){
                                $checked = "checked";
                            } else {
                                $checked = '';
                            }
                            ?>
                            <input type="checkbox" class="form-check-input" id="autoresponder_on" name="autoresponder_on" <?=$checked?>/>
                            <label class="form-check-label" for="autoresponder_on">Auto Responder Enabled</label>
                        </div>
                        <div class="form-group">
                            <label for="autoresponder_subject">Auto Responder Subject</label>
                            <input type="text" class="form-control" id="autoresponder_subject" name="autoresponder_subject" placeholder="Auto Responder Subject" value="<?=$emailAccount['autoresponder_subject']?>"/>
                        </div>
                        <div class="form-group">
                            <label for="autoresponder_message">Auto Responder Message</label>
                            <textarea class="form-control" id="autoresponder_message" name="autoresponder_message" placeholder="Auto Responder Message"><?=$emailAccount['autoresponder_message']?></textarea>
                        </div>
                        <?php
                            if($emailAccount['autoresponder_from'] == ""){
                                $from = $emailAdress;
                            } else {
                                $from = $emailAccount['autoresponder_from'];
                            }
                        ?>
                        <div class="form-group">
                            <label for="autoresponder_from">Auto Responder From</label>
                            <input type="text" class="form-control" id="autoresponder_from" name="autoresponder_from" placeholder="Auto Responder From" value="<?=$from?>"/>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
    printFooter();