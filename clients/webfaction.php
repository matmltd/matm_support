<?php
	/**
	 * Created by PhpStorm.
	 * User: jake
	 * Date: 04/06/2018
	 * Time: 16:07
	 */

	namespace Webfaction;

	require_once ('vendor/autoload.php');

	use PhpXmlRpc\Value;
	use PhpXmlRpc\Request;
	use PhpXmlRpc\Client;


	class webfaction {

		/* Base url for API Methods */
		private $base = "https://api.webfaction.com";

		/* Version number for API Calls */
		private $version = 2;

		/* Initialise the client var */
		private $client;

		/* Set Machine name for server */
		private $machine = 'Web533';

		private $token;

		private $user;

		private $siteName;

		private $siteID;

		private $domains;
		private $email_addresses;

		public function __construct( $username, $password, $auth ) {

			$this->client = new Client( $this->base );

			//$this->client->setDebug(1);

			$this->user = $auth->getCurrentUser();

			//if(!isset($_SESSION['accessToken'])) {
			$this->token             = $this->login( $username, $password );
			$_SESSION['accessToken'] = $this->token;
			//} else {
			//	$this->token = $_SESSION['accessToken'];
			//}

			$this->GetSite();
		}

		/**
		 * @param $username
		 * @param $password
		 *
		 * @return string
		 */
		public function login( $username, $password ) {
			/*			$msg = new Request('login', array(new Value($username,'string'), new Value($password,'string'), new Value($this->machine,'string'), new Value($this->version,'int')));
						$response = $this->send($msg);
						echo "<pre>";
						var_dump($response->raw_data);
						echo "</pre>";
						$values = $response->value();

						return str_replace("\n", "", $values[0]->serialize());*/

			return str_replace( "\n", "", shell_exec( "python session_id.py" ) );

		}


		/**
		 * @param $msg
		 *
		 * @return \PhpXmlRpc\Response|\PhpXmlRpc\Response[]
		 */
		public function send( $msg, $die = 1 ) {
			$resp = $this->client->send( $msg );

			if ( $die == 1 ) {
				if ( $resp->faultCode() == 0 ) {
					return $resp;
				} else {
					die( 'Error From XMLRPC Server: ' . $resp->faultString() );
				}
			} else {
					return $resp;
			}

		}

		public function GetSite() {
			$this->domains = array();

			$msg = new Request( 'list_websites', array( new Value( $this->token, 'string' ) ) );

			$response = $this->send( $msg );

			$values = $response->value();

			$siteID = $this->user['website_id'];

			foreach ( $values as $value ) {

				$id   = $value->me['struct']['id']->me['int'];
				$name = $value->me['struct']['name']->me['string'];

				if ( $siteID == $id ) {
					$Alldomains = $value->me['struct']['subdomains']->me;
					foreach ( $Alldomains as $domains ) {
						foreach ( $domains as $domain ) {
							$this->domains[] = $domain->me['string'];
						}
					}

					$this->siteID   = $id;
					$this->siteName = $name;
					break;
				}
			}
		}

		public function get_all_emailAddresses() {

			$this->email_addresses = array();

			$msg = new Request( 'list_emails', array( new Value( $this->token, 'string' ) ) );

			$response = $this->send( $msg );

			$values      = $response->value();
			$allAccounts = array();
			foreach ( $values as $value ) {
				$emailAddress = $value->me['struct']['email_address']->me['string'];
				preg_match_all( "/(.*?)@(.*?)$/", $emailAddress, $found );

				$domain = $found[2][0];
				if ( in_array( $domain, $this->domains ) ) {
					$this->email_addresses[]                   = $emailAddress;
					$targets                                   = $value->me['struct']['targets']->me['string'];
					$autoresponder_on                          = $value->me['struct']['autoresponder_on'][0];
					$autoresponder_subject                     = $value->me['struct']['autoresponder_subject'][0];
					$autoresponder_message                     = $value->me['struct']['autoresponder_message'][0];
					$autoresponder_from                        = $value->me['struct']['autoresponder_from'][0];
					$script_machine                            = "";
					$script_path                               = "";
					$allAccounts[ urlencode( $emailAddress ) ] = [
						'targets'               => $targets,
						'autoresponder_on'      => $autoresponder_on,
						'autoresponder_subject' => $autoresponder_subject,
						'autoresponder_message' => $autoresponder_message,
						'autoresponder_from'    => $autoresponder_from,
						'script_machine'        => $script_machine,
						'script_path'           => $script_path
					];
				}
			}

			return $allAccounts;
		}

		public function get_email_account_from_address( $emailAddress ) {
			$emailAccounts = $this->get_all_emailAddresses();

			if(isset($emailAccounts[urlencode($emailAddress)])){
				return $emailAccounts[urlencode($emailAddress)];
			} else {
				return false;
			}
		}

		public function enable_forwarder( $emailAddress, $state ) {
			$account = $this->get_email_account_from_address($emailAddress);

			if($state == "true"){
				$state = true;
			} else {
				$state = false;
			}
			$resp = $this->updateEmailAddress($emailAddress, [$account['targets']], $state, $account['autoresponder_subject'], $account['autoresponder_message'],$account['autoresponder_from'], $account['script_machine'], $account['script_path']);

			return $resp;
		}

		public function getEmailAddresses() {
			$this->get_all_emailAddresses();

			return $this->email_addresses;
		}

		public function getAllEmailAccounts() {
			$accounts = $this->get_all_emailAddresses();

			return $accounts;
		}

		public function addForwarder( $emailAddress ) {

		}

		public function updateEmailAddress( $email_addresses, $targets, $autoresponder_on, $autoresponder_subject, $autoresponder_message, $autoresponder_from, $script_machine = "", $script_path = "" ) {
			foreach ( $targets as $target ) {
				$targetsArray[] = new Value( $target, 'string' );
			}
			$msg = new Request( 'update_email', array(
				new Value( $this->token, 'string' ),
				new Value( $email_addresses, 'string' ),
				new Value( $target, 'string' ),
				new Value( $autoresponder_on, 'boolean' ),
				new Value( $autoresponder_subject, 'string' ),
				new Value( $autoresponder_message, 'string' ),
				new Value( $autoresponder_from, 'string' ),
				new Value( $script_machine, 'string' ),
				new Value( $script_path, 'string' )
			) );

			$response = $this->send( $msg, 0 );

			if ( $response->faultCode() != 0 ) {
				return false;
			} else {
				$values = $response->value();

				return $values;
			}

		}

		public function updateMailboxPassword( $mailbox, $password ) {
			$msg = new Request( 'change_mailbox_password', array( new Value( $this->token, 'string' ), new Value( $mailbox, 'string' ), new Value( $password, 'string' ) ) );

			$response = $this->send( $msg, 0 );

			if ( $response->faultCode() != 0 ) {
				return false;
			} else {
				$values = $response->value();

				return $values;
			}

		}

		public function getMailboxes() {

			$emailAddresses = $this->getAllEmailAccounts();
			
			foreach ($emailAddresses as $email_address) {
				$targets[] = $email_address['targets'];
			}

			$msg = new Request( 'list_mailboxes', array( new Value( $this->token, 'string' ) ) );

			$response = $this->send( $msg );

			$values      = $response->value();
			$allMailboxes = [];
			foreach ( $values as $value ) {
				$mailbox = $value->me['struct']['mailbox']->me['string'];
				if ( in_array( $mailbox, $targets ) ) {
					$allMailboxes[] = $mailbox;
				}
			}

			return $allMailboxes;
		}
	}