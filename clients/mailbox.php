<?php
	/**
	 * Created by PhpStorm.
	 * User: jake
	 * Date: 04/06/2018
	 * Time: 16:06
	 */

	session_start();

	ini_set('display_errors', 1);
	date_default_timezone_set('Etc/UTC');

	require_once ('webfaction.php');
	require_once ('functions.php');

	auth();

	$webfaction = new \Webfaction\webfaction('matm', 'w$@q62y-6kiap$uf', $auth);

	$mailboxes = $webfaction->getMailboxes();

	printHeader();
?>


	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">Email Accounts</div>

					<div class="panel-body">
						<?php
							foreach($mailboxes as $mailbox){
								?>
								<div class="account">
									<div class="acount_inner">
										<h3><?=$mailbox?></h3>
										<p><a href="editMailbox.php?id=<?= urlencode( $mailbox ) ?>">Change Password</a></p>
									</div>
								</div>
								<?php
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php
	printFooter();