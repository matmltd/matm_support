<?php
	/**
	 * Created by PhpStorm.
	 * User: jake
	 * Date: 04/06/2018
	 * Time: 16:06
	 */

	session_start();

	ini_set('display_errors', 1);
	date_default_timezone_set('Etc/UTC');

	require_once ('webfaction.php');
	require_once ('functions.php');

	//auth();
	
	$webfaction = new \Webfaction\webfaction('matm', 'w$@q62y-6kiap$uf', $auth);

	$emailAdresses = $webfaction->getAllEmailAccounts();

	printHeader();
?>

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Welcome</div>

				<div class="panel-body">
					<div class="container-fluid">
                        <div class="row">
                            <div class="col-md-5">
                                <a href="emails.php">
                                    <div class="button">
                                        <p>Auto Replies</p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-5 col-md-offset-2">
                                <a href="mailbox.php">
                                    <div class="button right">
                                        <p>Change Passwords</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
    printFooter();

