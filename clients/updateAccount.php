<?php
	/**
	 * Created by PhpStorm.
	 * User: jake
	 * Date: 07/06/2018
	 * Time: 10:03
	 */


	if(!isset($_POST)){
		header('Location: index.php');
	}

	if(!isset($_GET)){
		header('Location: index.php');
	}
	session_start();

	ini_set('display_errors', 1);
	date_default_timezone_set('Etc/UTC');

	require_once ('webfaction.php');
	require_once ('functions.php');

	auth();

	$webfaction = new \Webfaction\webfaction('matm', 'w$@q62y-6kiap$uf', $auth);

	$webfaction->get_all_emailAddresses();

	if($_GET['action'] == "update") {

		if ( isset( $_POST['emailAddress'] ) ) {
			$email_addresses = $_POST['emailAddress'];
		} else {
			print "Error: Email Address Not Set";
			exit;
		}

		if ( isset( $_POST['mailbox'] ) ) {
			$targets = [ $_POST['mailbox'] ];
		} else {
			print "Error: Mailbox Not Set";
			exit;
		}

		if ( isset( $_POST['autoresponder_on'] ) ) {
			$autoresponder_on = true;
		} else {
			$autoresponder_on = false;
		}

		if ( isset( $_POST['autoresponder_subject'] ) ) {
			$autoresponder_subject = $_POST['autoresponder_subject'];
		} else {
			print "Error: Subject Not Set";
			exit;
		}

		if ( isset( $_POST['autoresponder_message'] ) ) {
			$autoresponder_message = $_POST['autoresponder_message'];
		} else {
			print "Error: Message Not Set";
			exit;
		}

		if ( isset( $_POST['autoresponder_from'] ) ) {
			$autoresponder_from = $_POST['autoresponder_from'];
		} else {
			print "Error: From Not Set";
			exit;
		}

		if ( isset( $_POST['script_machine'] ) ) {
			$script_machine = $_POST['script_machine'];
		} else {
			$script_machine = "";
		}

		if ( isset( $_POST['script_path'] ) ) {
			$script_path = $_POST['script_path'];
		} else {
			$script_path = "";
		}

		$response = $webfaction->updateEmailAddress( $email_addresses, $targets, $autoresponder_on, $autoresponder_subject, $autoresponder_message, $autoresponder_from, $script_machine, $script_path );

		if ( $response === false ) {
			header( 'Location: editAccount.php?m=Error&id=' . $email_addresses );
		} else {
			header( 'Location: editAccount.php?m=Success&id=' . $email_addresses );
		}
	} elseif ($_GET['action'] == "create"){
		$response = $webfaction->createEmailAccount();
	} elseif ($_GET['action'] == "enableForwarder"){
		$emailAddress = urldecode($_GET['email']);
		$response = $webfaction->enable_forwarder($emailAddress, $_GET['checked']);
		if($response == false){
			print 0;
		} else {
			print 1;
		}
	}