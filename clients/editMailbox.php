<?php
	/**
	 * Created by PhpStorm.
	 * User: jake
	 * Date: 04/06/2018
	 * Time: 16:06
	 */

	session_start();

	ini_set('display_errors', 1);
	date_default_timezone_set('Etc/UTC');

	require_once ('webfaction.php');
	require_once ('functions.php');

	auth();

	$webfaction = new \Webfaction\webfaction('matm', 'w$@q62y-6kiap$uf', $auth);
	if(!isset($_GET['id'])){
		print "Error: Mailbox not found. Please go back and try again</br>";
		print "<a href='index.php'>Click here to go back Home</a>";
		exit;
	}
	$mailbox = urldecode($_GET['id']);
	$emailAdresses = $webfaction->getMailboxes();

	printHeader();
?>

	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">Edit Email Account - <?=$mailbox?></div>
					<div class="panel-body">
						<?php
						if(isset($_GET['m'])){
							if($_GET['m'] == "Success"){
								?>
								<div class="alert alert-success">
									<p><strong>Success!</strong> Your Password has been updated</p>
								</div>
								<?php
							} elseif ($_GET['m'] == "Error") {
								?>
								<div class="alert alert-danger">
									<p><strong>Error!</strong> Your Password has not updated. Please try again later</p>
								</div>
								<?php
							}
						}
						?>
						<form action="updateMailbox.php?action=updatePassword" method="POST">
							<div class="form-group">
								<label for="password">New Password</label>
								<input type="password" class="form-control" id="password" name="password" placeholder="Password"/>
							</div>
							<div class="form-group">
								<label for="passwordSecond">New Password Again</label>
								<input type="password" class="form-control" id="passwordSecond" name="passwordSecond" placeholder="Password Again"/>
							</div>
								<input type="hidden" class="form-control" id="mailbox" name="mailbox" placeholder="mailbox" value="<?=$mailbox?>"/>
							<button type="submit" class="btn btn-primary">Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php
	printFooter();