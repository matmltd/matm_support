<?php
	ini_set('display_errors', 1);
	include_once ('functions.php');
	$siteurl = "http://localhost/seo_review";
$body = '
<html>
<head>
	<link rel="stylesheet" href="http://support.matm.co.uk/riwal/riwal.css"/>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>

<body>
<nav class="nav header"><p></p></nav>
<div class="container paper">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="card">
						<h1 style="text-align: center;">SEO Review - Riwal</h1>
						<p style="text-align: center"><strong>April 2018</strong></p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h2 class="section_title">Key Stats</h2>
					<hr/>
				</div>
			</div>
			<div class="row">
				<div class="card">
					<div class="content">
						<table width="" border="0" class="keystats">
							<tr class="domain_stats_data">
								<td id="page_sessions">
									<img src="<?=$siteurl?>/img/chain-link.png"/>
									<p>Inbound Links:
										<br/>
										<strong class="sessions saving">41,402</strong>
									</p>
								</td>
								<td id="users">
									<img src="<?=$siteurl?>/img/chain-link.png"/>
									<p>Inbound Link Domains:
										<br/>
										<strong>569</strong>
									</p>


								</td>
								<td id="new_unique_users">
									<img src="<?=$siteurl?>/img/visibility.png"/>
									<p>Keywords Tracked:
										<br/>
										<strong>56</strong>
									</p>
								</td>
								<td id="pages_viewed">
									<img src="<?=$siteurl?>/img/visibility.png"/>
									<p>Keywords in top 10:
										<br/>
										<strong>2</strong>
									</p>
								</td>
								<td id="avg_pages_viewed">
									<img src="<?=$siteurl?>/img/visibility.png"/>
									<p>Keywords in top 100:
										<br/>
										<strong>14</strong>
									</p>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h2 class="section_title">Local Listings</h2>
					<hr/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="card">
						<div class="content">
							<table class="coloured">
								<tr>
									<td></td>
									<td>Google Maps</td>
									<td>Google Business</td>
									<td>Bing Places</td>
									<td>Yell</td>
								</tr>
								<tr class="answers">
									<td>Listed?</td>
									<td class="true">&#10004;</td>
									<td class="true">&#10004;</td>
									<td class="true">&#10004;</td>
									<td class="true">&#10004;</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h2 class="section_title">Rank Analysis</h2>
					<hr/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="description">
							<h3>What are keyword positions?</h3>
							<p>Keyword position refers to the position your website shows up in a google search for a specific keyword. The higher the Rank, the more visible you are when someone is searching, which in turn gives you the potential for more users on the website</p>
							<p>The purpose of SEO is to optimise your site so it shows up higher in the search rankings. Understanding where your site ranks currently is the first step</p>
							<h3>What is the Issue?</h3>
							<p>Currently, out of the 56 Keywords we are tracking, only 2 of them are in the top 10, and only 14 are in the top 100. This means that your website is much less visible to potential customers and so in turn you are missing out on those potential customers.</p>
						</div>
						<div class="row">
							<div class="col-md-6">
								<table class="padded">
									<tr>
										<td>Total Keywords Tracked:</td>
										<td>
											<p><strong>56</strong></p>
										</td>
									</tr>
									<tr>
										<td>Keywords in top 10:</td>
										<td>
											<p><strong>2</strong> (3.57%)</p>
										</td>
									</tr>
									<tr>
										<td>Keywords in top 100:</td>
										<td>
											<p><strong>14</strong> (25%)</p>
										</td>

									</tr>
								</table>
							</div>
							<div class="col-md-6">
								<table class="smaller">
									<p><strong>Top 5 Keywords</strong></p>
									<tr>
										<td style="text-align: center"><strong>Keyword</strong></td>
										<td><strong>Position</strong></td>
									</tr>
									<tr>
										<td>aerial work platform hire</td>
										<td><strong>7</strong></td>
									</tr>
									<tr>
										<td>aerial work platform</td>
										<td><strong>10</strong></td>
									</tr>
									<tr>
										<td>aerial work platforms</td>
										<td><strong>12</strong></td>
									</tr>
									<tr>
										<td>ultra booms</td>
										<td><strong>13</strong></td>
									</tr>
									<tr>
										<td>Access hire</td>
										<td><strong>54</strong></td>
									</tr>
								</table>
							</div>
						</div>
						<div class="description">
							<h3>How can MATM help?</h3>
							<p>Google rankings can be improved through On page and Off page SEO. The main issues that will need to be tackled in order to improve your rankings are listed in detail bellow, along with how we would go about fixing them.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h2 class="section_title">Inbound Links</h2>
					<hr/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="description">
							<h3>What are Inbound Links?</h3>
							<p>Inbound links are important for SEO, as they show search engines that your site is trusted and respected enough by other websites for them to include a link to your website.</p>
							<p>Although, an important thing to note is it\'s not all about quantity. Search engines prefer inbound links from other large, trusted websites as apposed to lots of spam links from smaller, less trusted websites</p>
							<h3>What is the problem?</h3>
							<p>The Riwal website is very strong in this area. Having a large amount of good quality back links, form domains with a high domain score. However, it is important to keep a flow a fresh Back Links to your site.</p>
						</div>
						<div class="row">
							<div class="col-md-6">
								<table class="padded">
									<tr>
										<td>Total Inbound Links:</td>
										<td>
											<p><strong>41,402</strong></p>
										</td>
									</tr>
									<tr>
										<td>Links from unique domains:</td>
										<td>
											<p><strong>569</strong></p>
										</td>
									</tr>
									<tr>
										<td>Link Spam Score</td>
										<td>
											<p><strong>2%</strong></p>
										</td>

									</tr>
								</table>
							</div>
							<div class="col-md-6">
								<table class="smaller">
									<p><strong>Top 5 Linking Domains</strong></p>
									<tr>
										<td style="text-align: center"><strong>Domain</strong></td>
									</tr>
									<tr>
										<td style="text-align: center">de.wikipedia.org</td>
									</tr>
									<tr>
										<td style="text-align: center">home.pl</td>
									</tr>
									<tr>
										<td style="text-align: center">mirror.co.uk</td>
									</tr>
									<tr>
										<td style="text-align: center">ask.com</td>
									</tr>
									<tr>
										<td style="text-align: center">viadeo.com</td>
									</tr>
								</table>
							</div>
						</div>
						<div class="description">
							<h3>How can MATM help?</h3>
							<p>Going forward, it is important to keep gaining fresh back links to your site from trusted sources. We would do this by Adding the website to 3 high domain score, trusted directories Per Month.</p>
							<p>These Directories include sites such as:</p>
							<ul>
								<li><a target="_blank" href="http://yellowpages.com">yellowpages.com</a></li>
								<li><a target="_blank" href="http://yellowpages.com">indeed.co.uk</a></li>
								<li><a target="_blank" href="http://192.com">192.com</a></li>
							</ul>
							<p>Doing this will keep a steady flow of back links coming in to the site, showing search engines that the site is still relevant and trusted which will help stop and slipping of keyword rankings</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h2 class="section_title">Page Meta Titles</h2>
					<hr/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="description">
							<h3>What are Page Meta Titles?</h3>
							<p>Meta Titles are what search engines use for the link text in a search result. If a meta title is not set, the search engine will use other data, such as headings or text in the page to set the title.</p>
							<p>Because of this, it is important to set titles for all pages, to ensure the search engine results a user sees are accurate and encourage them to click through to your site.</p>
							<p>It is also important to keep each page title unique, as search engines punish you for having too many duplicate titles on your site.</p>
							<h3>What is the problem?</h3>
							<p>On the Riwal site, there is an issue with duplicate Titles. A third of all the titles on the site are not unique, this is picked up by search engines and your rankings will be penalised because of it.</p>
							<p>Also, as well as the duplicates, most of the page titles are bellow the recommended size, and therefor are likely not as descriptive as they could be</p>
						</div>
						<div class="row">
							<div class="col-md-6">
								<table class="padded">
									<tr>
										<td>Meta Titles missing:</td>
										<td>
											<p><strong>0</strong> (0%)</p>
										</td>
									</tr>
									<tr>
										<td>Duplicate titles:</td>
										<td>
											<p><strong>26</strong> (33.33%)</p>
										</td>
									</tr>
									<tr>
										<td>Titles too long (Over 65 characters):</td>
										<td>
											<p><strong>3</strong> (3.85%)</p>
										</td>

									</tr>
								</table>
							</div>
							<div class="col-md-6">
								<table class="padded">
									<tr>
										<td>Titles too short (under 30 characters):</td>
										<td>
											<p><strong>64</strong> (82.05%)</p>
										</td>
									</tr>
									<tr>
										<td>Titles too long (over 568 pixels):</td>
										<td>
											<p><strong>2</strong> (2.56%)</p>
										</td>
									</tr>
									<tr>
										<td>Titles too short (under 200 pixels):</td>
										<td>
											<p><strong>62</strong> (79.49%)</p>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h2 class="section_title">Page Meta Descriptions</h2>
					<hr/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="description">
							<p>Similar to Meta Titles, Meta Descriptions are what search engines use for the description of your website in search results.</p>
							<p>It is also important to set a unique description for every page on your website. These descriptions should be a maximum of 156 characters long (or 940px) so they aren\'t truncated, and a minimum of around 70 characters long (or 400px)</p>
						</div>
						<div class="row">
							<div class="col-md-6">
								<table class="padded">
									<tr>
										<td>Meta Descriptions missing:</td>
										<td>
											<p><strong>52</strong> (66.67%)</p>
										</td>
									</tr>
									<tr>
										<td>Duplicate Meta Descriptions:</td>
										<td>
											<p><strong>4</strong> (5.13%)</p>
										</td>
									</tr>
									<tr>
										<td>Descriptions too long (Over 65 characters):</td>
										<td>
											<p><strong>13</strong> (16.67%)</p>
										</td>

									</tr>
								</table>
							</div>
							<div class="col-md-6">
								<table class="padded">
									<tr>
										<td>Descriptions too short (under 70 characters):</td>
										<td>
											<p><strong>55</strong> (70.51%)</p>
										</td>
									</tr>
									<tr>
										<td>Descriptions too long (over 940 pixels):</td>
										<td>
											<p><strong>8</strong> (10.26%)</p>
										</td>
									</tr>
									<tr>
										<td>Descriptions too short (under 400 pixels):</td>
										<td>
											<p><strong>55</strong> (70.51%)</p>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h2 class="section_title">Page Headings</h2>
					<hr/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="description">
							<p>Page headings allow search engines to get a picture of what the page is about, so it can be clarified for search results. The more accurate the headings are, the easier it is for the search engine to understand what the page is about</p>
						</div>





						<div class="row">
							<div class="col-md-6">
								<p style="text-align: center"><strong>Headings (H1)</strong></p>
								<table class="padded">
									<tr>
										<td>Page Headings Missing:</td>
										<td>
											<p><strong>18</strong> (23.08%)</p>
										</td>
									</tr>
									<tr>
										<td>Duplicate Page Headings:</td>
										<td>
											<p><strong>11</strong> (14.10%)</p>
										</td>
									</tr>
									<tr>
										<td>Page Headings Over 70 Characters:</td>
										<td>
											<p><strong>3</strong> (3.85%)</p>
										</td>
									</tr>
									<tr>
										<td>Multiple Page Headings on page:</td>
										<td>
											<p><strong>8</strong> (10.26%)</p>
										</td>
									</tr>
								</table>
							</div>
							<div class="col-md-6">
								<p style="text-align: center"><strong>Sub-Headings (H2)</strong></p>
								<table class="padded">
									<tr>
										<td>Sub-Headings Missing:</td>
										<td>
											<p><strong>56</strong> (71.79%)</p>
										</td>
									</tr>
									<tr>
										<td>Duplicate Sub-Headings:</td>
										<td>
											<p><strong>7</strong> (8.97%)</p>
										</td>
									</tr>
									<tr>
										<td>Sub-Headings Over 70 Characters:</td>
										<td>
											<p><strong>0</strong> (0.0%)</p>
										</td>
									</tr>
									<tr>
										<td>Multiple Sub-Headings on page:</td>
										<td>
											<p><strong>19</strong> (24.36%)</p>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h2 class="section_title">Images</h2>
					<hr/>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="description">
							<p>Having images set up correctly on your website is essential for SEO. Search engines can\'t see what the image is, so they rely on the image meta data, such as "alt" tags and "title" tags.</p>
							<p>It is also important to optimize image sizes so they don\'t slow the sites load times dramatically. You should aim to have images under 100kb in size where possible.</p>
						</div>
						<div class="row">
							<div class="col-md-6">
								<table class="padded">
									<tr>
										<td>Images Over 100 kb:</td>
										<td>
											<p><strong>33</strong> (39.29%)</p>
										</td>
									</tr>
									<tr>
										<td>Images with Missing Alt Text:</td>
										<td>
											<p><strong>70</strong> (83.33%)</p>
										</td>
									</tr>
									<tr>
										<td>Images Alt Text Over 100 Characters:</td>
										<td>
											<p><strong>2</strong> (2.38%)</p>
										</td>
									</tr>
								</table>
							</div>
							<div class="col-md-6">
								<table class="smaller">
									<p><strong>Top 5 largest images</strong></p>
									<tr>
										<td>Aerospace-600x300.ashx</td>
										<td>2114 KB</td>
									</tr>
									<tr>
										<td>JLG-600AJ---375x260.ashx</td>
										<td>756 KB</td>
									</tr>
									<tr>
										<td>TrackTrace2.ashx</td>
										<td>687 KB</td>
									</tr>
									<tr>
										<td>Construction-600x300.ashx</td>
										<td>290 KB</td>
									</tr>
									<tr>
										<td>Film-and-TV-600x300-Mk2.ashx</td>
										<td>248 KB</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<p>test</p>
</div>
</body>
</html>';

// To send HTML mail, the Content-type header must be set
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

// Create email headers
	$headers .= "From: jtc@matm.co.uk\r\n".
	            'Reply-To: jtc@matm.co.uk'."\r\n" .
	            'X-Mailer: PHP/' . phpversion();

var_dump(mail('jtc@matm.co.uk', 'testing emailer', $body, $headers));