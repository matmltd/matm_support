<?php
/**
 * Created by PhpStorm.
 * User: jake
 * Date: 26/09/2017
 * Time: 10:36
 */

if(session_status() != 2){
	session_start();
};
define('DebugMode', false);

include_once ('GoogleAnalyticsAPI.class.php');
//include_once ('conversion-rate-functions.php');

	if(!isset($_GET['s'])){
		if(isset($_SESSION['s'])){
			$_GET['s'] = $_SESSION['s'];
		}
	}
	if(!isset($_GET['e'])){
		if(isset($_SESSION['e'])){
			$_GET['e'] = $_SESSION['e'];
		}
	}
	$ga = new GoogleAnalyticsAPI('service');


$goalID = 1;

$conversionsURL = 1;

if (!file_exists('cache/' . Tempuserid)) {
	mkdir('cache/' .  Tempuserid, 0777, true);
}

function  connect() {
	global $conversionsURL;
	global $goalID;
	global $ga;
	if ( ! isset( $_SESSION['tokenExpires'] ) || ! isset( $_SESSION['tokenCreated'] ) || time() >= ( $_SESSION['tokenCreated'] + $_SESSION['tokenExpires'] ) ) {
		if ( DebugMode == true ) {
			echo "<pre>";
			var_dump( $_SESSION );
			echo "</pre>";
		}
		$ga->auth->setClientId( '786826102984-o69344c8iqoph9c2j4a3defh7g3k722r.apps.googleusercontent.com' ); // From the APIs console
		$ga->auth->setEmail( '786826102984-o69344c8iqoph9c2j4a3defh7g3k722r@developer.gserviceaccount.com' ); // From the APIs console
		$ga->auth->setPrivateKey( 'My Project-67eec1ff0b85.p12' ); // Path to the .p12 file

		$auth = $ga->auth->getAccessToken();
// Try to get the AccessToken
		if ( $auth['http_code'] == 200 ) {
			$_SESSION['accessToken']  = $auth['access_token'];
			$_SESSION['tokenExpires'] = $auth['expires_in'];
			$_SESSION['tokenCreated'] = time();

			return true;
		} else {
			return false;
		}
	} else {
		return true;
	}
}

function  get_visitors(){
	global $conversionsURL;
	global $goalID;
    global $ga;
    global $db;
    if(file_exists("cache/" . Tempuserid . "/visitors")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
		$Cache = file_get_contents("cache/" . Tempuserid . "/visitors");
	 } else {
		 fopen("cache/" . Tempuserid . "/visitors", 'w');
		 $Cache = "";
    }

    if($Cache != "" && DebugMode != true &&filemtime("cache/" . Tempuserid . "/visitors") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
        return json_decode($Cache, true);
    } else {
        print (DebugMode ? "grabbing" : "");
        if (connect()) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . Tempuserid)[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['e'])) {
                if (isset($_GET['s'])) {
                    if ($_GET['s'] > $_GET['e']) {
                        print "	<script type=\"text/javascript\">
                                $(document).ready(function(){ 
                        
                                    demo.initChartist();
                        
                                    $.notify({
                                        icon: 'pe-7s-gift',
                                        message: \"Start Date cannot be before end date!\"
                        
                                    },{
                                        type: 'info', 
                                        timer: 4000
                                    });
                        
                                });
                            </script>";
                        $_GET['s'] = date('Y-m-d', strtotime('-30 days'));
                        $_GET['e'] = date('Y-m-d');
                    } else {
                       /* if (date('Y-m-d', strtotime('-30 days')) > $_GET['e']) {
                            print "	<script type=\"text/javascript\">
                                $(document).ready(function(){
                        
                                    demo.initChartist();
                        
                                    $.notify({
                                        icon: 'pe-7s-gift',
                                        message: \"Start Date cannot be before end date!\"
                        
                                    },{
                                        type: 'info',
                                        timer: 4000
                                    });
                        
                                });
                            </script>";
                            $_GET['s'] = strtotime('-30 days');
                            $_GET['e'] = time();
                        }*/
                    }
                }
            }

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-30 days'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );
            $ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
            $params = array(
                'metrics' => 'ga:visits',
                'dimensions' => 'ga:date',
            );
            $visits = $ga->query($params);
            $return = [];
            foreach ($visits['rows'] as $visit) {
	            $date            = $visit[0];
	            $return[ $date ] = $visit[1];
            }
	        if(!isset($_GET['s']) || !isset($_GET['e'])) {
		        file_put_contents( "cache/" . Tempuserid . "/visitors", json_encode( $return ) );
	        }
            return $return;
        } else {
            return false;
        }
    }
}

function  get_users(){
	global $conversionsURL;
	global $goalID;
    global $ga;
    global $db;
    if(file_exists("cache/" . Tempuserid . "/users")) {
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
		$Cache = file_get_contents("cache/" . Tempuserid . "/users");
	 } else {
		 fopen("cache/" . Tempuserid . "/users", 'w');
		 $Cache = "";
    }

    if($Cache != "" && DebugMode != true &&filemtime("cache/" . Tempuserid . "/users") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
        return json_decode($Cache, true);
    } else {
        print (DebugMode ? "grabbing" : "");
        if (connect()) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . Tempuserid)[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-30 days'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );
            $ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
            $params = array(
                'metrics' => 'ga:users'
            );
            $users = $ga->query($params);
	        if(!isset($_GET['s']) || !isset($_GET['e'])) {
		        file_put_contents( "cache/" . Tempuserid . "/users", json_encode( $users['rows'][0][0] ) );
	        }
            return $users['rows'][0][0];

        } else {
            return false;
        }
    }
}

function  get_page_visits(){
	global $conversionsURL;
	global $goalID;
	global $ga;
	global $db;
	if(file_exists("cache/" . Tempuserid . "/page_visits")) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents("cache/" . Tempuserid . "/page_visits");
	} else {
		fopen("cache/" . Tempuserid . "/page_visits", 'w');
		$Cache = "";
	}

	if($Cache != "" && DebugMode != true &&filemtime("cache/" . Tempuserid . "/page_visits") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
		return json_decode($Cache, true);
	} else {
		print (DebugMode ? "grabbing" : "");
		if (connect()) {
			// Set the accessToken and Account-Id
			$ga->setAccessToken($_SESSION['accessToken']);
			$id = $db->query("SELECT propertyId FROM users WHERE id=" . Tempuserid)[0]['propertyId'];
			$ga->setAccountId($id);

			if (isset($_GET['s'])) {
				$start = date('Y-m-d', $_GET['s']);
			} else {
				$start = date('Y-m-d', strtotime('-30 days'));
			}

			if (isset($_GET['e'])) {
				$end = date('Y-m-d', $_GET['e']);
			} else {
				$end = date('Y-m-d');
			}


			$defaults = array(
				'start-date' => $start,
				'end-date' => $end,
			);
			$ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
			$params = array(
				'metrics' => 'ga:pageviews'
			);
			$users = $ga->query($params);
			if(!isset($_GET['s']) || !isset($_GET['e'])) {
				file_put_contents( "cache/" . Tempuserid . "/page_visits", json_encode( $users['rows'][0][0] ) );
			}
			return $users['rows'][0][0];

		} else {
			return false;
		}
	}
}

function  get_session_duration(){
	global $conversionsURL;
	global $goalID;
	global $ga;
	global $db;
	if(file_exists("cache/" . Tempuserid . "/session_duration")) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents("cache/" . Tempuserid . "/session_duration");
	} else {
		fopen("cache/" . Tempuserid . "/session_duration", 'w');
		$Cache = "";
	}

	if($Cache != "" && DebugMode != true &&filemtime("cache/" . Tempuserid . "/session_duration") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
		return json_decode($Cache, true);
	} else {
		print (DebugMode ? "grabbing" : "");
		if (connect()) {
			// Set the accessToken and Account-Id
			$ga->setAccessToken($_SESSION['accessToken']);
			$id = $db->query("SELECT propertyId FROM users WHERE id=" . Tempuserid)[0]['propertyId'];
			$ga->setAccountId($id);

			if (isset($_GET['s'])) {
				$start = date('Y-m-d', $_GET['s']);
			} else {
				$start = date('Y-m-d', strtotime('-30 days'));
			}

			if (isset($_GET['e'])) {
				$end = date('Y-m-d', $_GET['e']);
			} else {
				$end = date('Y-m-d');
			}


			$defaults = array(
				'start-date' => $start,
				'end-date' => $end,
			);
			$ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
			$params = array(
				'metrics' => 'ga:avgSessionDuration'
			);
			$users = $ga->query($params);
			if(!isset($_GET['s']) || !isset($_GET['e'])) {
				file_put_contents( "cache/" . Tempuserid . "/session_duration", json_encode( $users['rows'][0][0] ) );
			}
			return gmdate("i:s",$users['rows'][0][0]);

		} else {
			return false;
		}
	}
}

function  get_devices(){
	global $conversionsURL;
	global $goalID;
	global $ga;
	global $db;
	if(file_exists("cache/" . Tempuserid . "/devices")) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents("cache/" . Tempuserid . "/devices");
	} else {
		fopen("cache/" . Tempuserid . "/devices", 'w');
		$Cache = "";
	}

	if($Cache != "" && DebugMode != true &&filemtime("cache/" . Tempuserid . "/devices") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
		return json_decode($Cache, true);
	} else {
		print (DebugMode ? "grabbing" : "");
		if (connect()) {
			// Set the accessToken and Account-Id
			$ga->setAccessToken($_SESSION['accessToken']);
			$id = $db->query("SELECT propertyId FROM users WHERE id=" . Tempuserid)[0]['propertyId'];
			$ga->setAccountId($id);

			if (isset($_GET['s'])) {
				$start = date('Y-m-d', $_GET['s']);
			} else {
				$start = date('Y-m-d', strtotime('-30 days'));
			}

			if (isset($_GET['e'])) {
				$end = date('Y-m-d', $_GET['e']);
			} else {
				$end = date('Y-m-d');
			}


			$defaults = array(
				'start-date' => $start,
				'end-date' => $end,
			);
			$ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
			$params = array(
				'metrics' => 'ga:sessions',
				'dimensions' => 'ga:deviceCategory'
			);
			$GAdevices = $ga->query($params);
			$devices = array();
			$count = 0;
			foreach($GAdevices['rows'] as $theDevices) {
				$devices[$theDevices[0]] = $theDevices[1];
				$count += $theDevices[1];
			}
			$devices['all'] = $count;

			if(!isset($_GET['s']) || !isset($_GET['e'])) {
				file_put_contents( "cache/" . Tempuserid . "/devices", json_encode( $devices ) );
			}
			return $devices;

		} else {
			return false;
		}
	}
}

function  get_avg_Bounces(){
	global $conversionsURL;
	global $goalID;
	global $ga;
	global $db;
	if(file_exists("cache/" . Tempuserid . "/avg_Bounces")) {
		if ( DebugMode == true ) {
			print "Fetching Cache";
		}
		$Cache = file_get_contents("cache/" . Tempuserid . "/avg_Bounces");
	} else {
		fopen("cache/" . Tempuserid . "/avg_Bounces", 'w');
		$Cache = "";
	}

	if($Cache != "" && DebugMode != true &&filemtime("cache/" . Tempuserid . "/avg_Bounces") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
		return json_decode($Cache, true);
	} else {
		print (DebugMode ? "grabbing" : "");
		if (connect()) {
			// Set the accessToken and Account-Id
			$ga->setAccessToken($_SESSION['accessToken']);
			$id = $db->query("SELECT propertyId FROM users WHERE id=" . Tempuserid)[0]['propertyId'];
			$ga->setAccountId($id);

			if (isset($_GET['s'])) {
				$start = date('Y-m-d', $_GET['s']);
			} else {
				$start = date('Y-m-d', strtotime('-30 days'));
			}

			if (isset($_GET['e'])) {
				$end = date('Y-m-d', $_GET['e']);
			} else {
				$end = date('Y-m-d');
			}


			$defaults = array(
				'start-date' => $start,
				'end-date' => $end,
			);
			$ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
			$params = array(
				'metrics' => 'ga:bounceRate',
			);
			$bounce = $ga->query($params);

			if(!isset($_GET['s']) || !isset($_GET['e'])) {
				file_put_contents( "cache/" . Tempuserid . "/avg_Bounces", json_encode( round($bounce['rows'][0][0], 2) ));
			}
			return round($bounce['rows'][0][0], 2);

		} else {
			return false;
		}
	}
}

	function  get_new_users(){
		global $conversionsURL;
		global $goalID;
		global $ga;
		global $db;
		if(file_exists("cache/" . Tempuserid . "/newUsers")) {
			if ( DebugMode == true ) {
				print "Fetching Cache";
			}
			$Cache = file_get_contents("cache/" . Tempuserid . "/newUsers");
		} else {
			fopen("cache/" . Tempuserid . "/newUsers", 'w');
			$Cache = "";
		}

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . Tempuserid . "/newUsers") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
			return json_decode($Cache, true);
		} else {
			print (DebugMode ? "grabbing" : "");
			if (connect()) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken($_SESSION['accessToken']);
				$id = $db->query("SELECT propertyId FROM users WHERE id=" . Tempuserid)[0]['propertyId'];
				$ga->setAccountId($id);

				if (isset($_GET['s'])) {
					$start = date('Y-m-d', $_GET['s']);
				} else {
					$start = date('Y-m-d', strtotime('-30 days'));
				}

				if (isset($_GET['e'])) {
					$end = date('Y-m-d', $_GET['e']);
				} else {
					$end = date('Y-m-d');
				}


				$defaults = array(
					'start-date' => $start,
					'end-date' => $end,
				);
				$ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
				$params = array(
					'metrics' => 'ga:newUsers'
				);
				$users = $ga->query($params);
				if(!isset($_GET['s']) || !isset($_GET['e'])) {
					file_put_contents( "cache/" . Tempuserid . "/newUsers", json_encode( $users['rows'][0][0] ) );
				}
				return $users['rows'][0][0];

			} else {
				return false;
			}
		}
	}


function  get_tech(){
	global $conversionsURL;
	global $goalID;
    global $ga;
    global $db;
    if(file_exists("cache/" . Tempuserid . "/tech")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	} 
 $Cache = file_get_contents("cache/" . Tempuserid . "/tech"); 
 } else { 
 //fopen("cache/" . Tempuserid . "/tech" 'w'); 
 $Cache = ""; 
 }

    if($Cache != "" && DebugMode != true &&filemtime("cache/" . Tempuserid . "/tech") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
        return json_decode($Cache, true);
    } else {
        print (DebugMode ? "grabbing" : "");
        if (connect()) {
            // Set the accessToken and Account-Id

            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . Tempuserid)[0]['propertyId'];

            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-30 days'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );
            $ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
            $params = array(
                'metrics' => 'ga:visits',
                'dimensions' => 'ga:browser',
            );
            $browsers = $ga->query($params);

            $return = [];
            foreach ($browsers['rows'] as $browserStat) {
                $browser = $browserStat[0];
                $return[$browser] = $browserStat[1];
            }
            if(!isset($_GET['s']) || !isset($_GET['e'])) {
	            file_put_contents( "cache/" . Tempuserid . "/tech", json_encode( $return ) );
            }
            return $return;
        } else {
            return false;
        }
    }
}

function  get_conversion_rates(){
	global $conversionsURL;
	global $goalID;
    global $ga;
    global $db;
    global $db;
    if(file_exists("cache/" . Tempuserid . "/convRates")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	} 
        $Cache = file_get_contents("cache/" . Tempuserid . "/convRates"); 
    } else { 
        fopen("cache/" . Tempuserid . "/convRates", 'w');
        $Cache = "";
    }

    if($Cache != "" && DebugMode != true &&filemtime("cache/" . Tempuserid . "/convRates") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
        return json_decode($Cache, true);
    } else {
        print (DebugMode ? "grabbing" : "");

        if (connect()) {

            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . Tempuserid)[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-30 days'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d',$_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            $params = array(
                'metrics' => 'ga:adCost',
                'dimensions' => 'ga:date',
            );

            $costs = $ga->query($params);


            $costsArray = [];
            foreach ($costs['rows'] as $cost) {
                $Day = $cost[0];
                $costsArray[$Day] = $cost[1];
            }
            
            /* CONVERSIONS */

            $params = array(
                'metrics' => 'ga:goal' . $goalID . 'Completions',
                'dimensions' => 'ga:date',
            );

            $goals = $ga->query($params);
            $goalsArray = [];
            foreach ($goals['rows'] as $goal) {
                $Day = $goal[0];
                $goalsArray[$Day] = $goal[1];
            }


            $cpc = [];
            foreach ($costsArray as $date => $cost) {
                if ($goalsArray[$date] == 0 || $cost == 0) {
                    $cpc[$date] = 0;
                } else {
                    $cpc[$date] = $cost / $goalsArray[$date];
                }
            }
            if(!isset($_GET['s']) || !isset($_GET['e'])) {
	            file_put_contents( "cache/" . Tempuserid . "/convRates", json_encode( $cpc ) );
            }
            return $cpc;
        } else {
            return false;
        }
    }
}

function  get_conversion_average(){
	global $conversionsURL;
	global $goalID;
    global $ga;
    global $db;
    if(file_exists("cache/" . Tempuserid . "/convAvg")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	} 
        $Cache = file_get_contents("cache/" . Tempuserid . "/convAvg");
    } else {
        //fopen("cache/" . Tempuserid . "/convAvg" 'w');
        $Cache = "";
    }

    if($Cache != "" && DebugMode != true &&filemtime("cache/" . Tempuserid . "/convAvg") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
        return $Cache;
    } else {
        print (DebugMode ? "grabbing" : "");
        if (connect()) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . Tempuserid)[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-30 days'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }

            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            $params = array(
                'metrics' => 'ga:adCost',
            );

            $costs = $ga->query($params);

            $overallCost = $costs['totalsForAllResults']['ga:adCost'];

            /* CONVERSIONS */

            $params = array(
                'metrics' => 'ga:goal' . $goalID . 'Completions',
            );
            

            $goals = $ga->query($params);

            $conversions = $goals['totalsForAllResults']['ga:goal' . $goalID . 'Completions'];
            if ($overallCost == 0 || $conversions == 0) {

                if(!isset($_GET['s']) || !isset($_GET['e'])) {
	                file_put_contents( "cache/" . Tempuserid . "/convAvg", 0 );
                }

                return 0;
            } else {
                if(!isset($_GET['s']) || !isset($_GET['e'])) {
	                file_put_contents( "cache/" . Tempuserid . "/convAvg", $overallCost / $conversions );
                }
                return $overallCost / $conversions;
            }

        }
    }
}


function  get_Percent_conversion_rates(){
	global $conversionsURL;
	global $goalID;
    global $ga;
    global $db;
    if(file_exists("cache/" . Tempuserid . "/percentConvRates")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	} 
 $Cache = file_get_contents("cache/" . Tempuserid . "/percentConvRates"); 
 } else { 
 fopen("cache/" . Tempuserid . "/percentConvRates", 'w');
 $Cache = ""; 
 }

    if($Cache != "" && DebugMode != true &&filemtime("cache/" . Tempuserid . "/percentConvRates") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
        return json_decode($Cache, true);
    } else {
        print (DebugMode ? "grabbing" : "");
        if (connect()) {
            // Set the accessToken and Account-Ids
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . Tempuserid)[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-30 days'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            /*$params = array(
                'metrics' => 'ga:users',
                'dimensions' => 'ga:date',
            );

            $costs = $ga->query($params);

            $costsArray = [];
            foreach ($costs['rows'] as $cost) {
                $Day = $cost[0];
                $costsArray[$Day] = $cost[1];
            }*/

            /* CONVERSIONS */

            $params = array(
                'metrics' => 'ga:goal' . $goalID . 'ConversionRate',
                'dimensions' => 'ga:date',
            );

            $goals = $ga->query($params);
            $goalsArray = [];
            foreach ($goals['rows'] as $goal) {
                $Day = $goal[0];
                $goalsArray[$Day] = $goal[1];
            }
            if(!isset($_GET['s']) || !isset($_GET['e'])) {
	            file_put_contents( "cache/" . Tempuserid . "/percentConvRates", json_encode( $goalsArray ) );
            }
            return $goalsArray;
        } else {
            return false;
        }
    }
}

	function get_clicks()
	{
		global $ga;
		global $db;
		if(file_exists("cache/" . Tempuserid . "/clicks")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
            $Cache = file_get_contents("cache/" . Tempuserid . "/clicks");
        } else {
            fopen("cache/" . Tempuserid . "/clicks", 'w');
            $Cache = "";
        }

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . Tempuserid . "/clicks") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
			return json_decode($Cache, true);
		} else {
			print (DebugMode ? "grabbing" : "");
			if (connect()) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken($_SESSION['accessToken']);
				$id = $db->query("SELECT propertyId FROM users WHERE id=" . Tempuserid)[0]['propertyId'];
				$ga->setAccountId($id);

				if (isset($_GET['e'])) {
					if (isset($_GET['s'])) {
						if ($_GET['s'] > $_GET['e']) {
							print "	<script type=\"text/javascript\">
                                $(document).ready(function(){ 
                        
                                    demo.initChartist();
                        
                                    $.notify({
                                        icon: 'pe-7s-gift',
                                        message: \"Start Date cannot be before end date!\"
                        
                                    },{
                                        type: 'info', 
                                        timer: 4000
                                    });
                        
                                });
                            </script>";
							$_GET['s'] = date('Y-m-d', strtotime('-30 days'));
							$_GET['e'] = date('Y-m-d');
						} else {
							/* if (date('Y-m-d', strtotime('-30 days')) > $_GET['e']) {
								 print "	<script type=\"text/javascript\">
									 $(document).ready(function(){

										 demo.initChartist();

										 $.notify({
											 icon: 'pe-7s-gift',
											 message: \"Start Date cannot be before end date!\"

										 },{
											 type: 'info',
											 timer: 4000
										 });

									 });
								 </script>";
								 $_GET['s'] = strtotime('-30 days');
								 $_GET['e'] = time();
							 }*/
						}
					}
				}

				if (isset($_GET['s'])) {
					$start = date('Y-m-d', $_GET['s']);
				} else {
					$start = date('Y-m-d', strtotime('-30 days'));
				}

				if (isset($_GET['e'])) {
					$end = date('Y-m-d', $_GET['e']);
				} else {
					$end = date('Y-m-d');
				}


				$defaults = array(
					'start-date' => $start,
					'end-date' => $end,
				);
				$ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
				$params = array(
					'metrics' => 'ga:adClicks',
					'dimensions' => 'ga:date',
				);
				$clicks = $ga->query($params);
				$return = [];

				foreach ($clicks['rows'] as $visit) {
					$date = $visit[0];
					$return[$date] = $visit[1];
				}
				if(!isset($_GET['s']) || !isset($_GET['e'])) {
					file_put_contents( "cache/" . Tempuserid . "/clicks", json_encode( $return ) );
				}
				return $return;
			} else {
				return false;
			}
		}
	}

	function get_spend()
	{
		global $ga;
		global $db;
		if(file_exists("cache/" . Tempuserid . "/spend")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
			$Cache = file_get_contents("cache/" . Tempuserid . "/spend");
        } else {
            fopen("cache/" . Tempuserid . "/spend", 'w');
            $Cache = "";
        }

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . Tempuserid . "/spend") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
			return json_decode($Cache, true);
		} else {
			print (DebugMode ? "grabbing" : "");
			if (connect()) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken($_SESSION['accessToken']);
				$id = $db->query("SELECT propertyId FROM users WHERE id=" . Tempuserid)[0]['propertyId'] ;
				$ga->setAccountId($id);

				if (isset($_GET['e'])) {
					if (isset($_GET['s'])) {
						if ($_GET['s'] > $_GET['e']) {
							print "	<script type=\"text/javascript\">
                                $(document).ready(function(){ 
                        
                                    demo.initChartist();
                        
                                    $.notify({
                                        icon: 'pe-7s-gift',
                                        message: \"Start Date cannot be before end date!\"
                        
                                    },{
                                        type: 'info', 
                                        timer: 4000
                                    });
                        
                                });
                            </script>";
							$_GET['s'] = date('Y-m-d', strtotime('-30 days'));
							$_GET['e'] = date('Y-m-d');
						} else {
							/* if (date('Y-m-d', strtotime('-30 days')) > $_GET['e']) {
								 print "	<script type=\"text/javascript\">
									 $(document).ready(function(){

										 demo.initChartist();

										 $.notify({
											 icon: 'pe-7s-gift',
											 message: \"Start Date cannot be before end date!\"

										 },{
											 type: 'info',
											 timer: 4000
										 });

									 });
								 </script>";
								 $_GET['s'] = strtotime('-30 days');
								 $_GET['e'] = time();
							 }*/
						}
					}
				}

				if (isset($_GET['s'])) {
					$start = date('Y-m-d', $_GET['s']);
				} else {
					$start = date('Y-m-d', strtotime('-30 days'));
				}

				if (isset($_GET['e'])) {
					$end = date('Y-m-d', $_GET['e']);
				} else {
					$end = date('Y-m-d');
				}


				$defaults = array(
					'start-date' => $start,
					'end-date' => $end,
				);
				$ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
				$params = array(
					'metrics' => 'ga:adCost',
					'dimensions' => 'ga:date',
				);
				$clicks = $ga->query($params);
				$return = [];

				foreach ($clicks['rows'] as $visit) {
					$date = $visit[0];
					$return[$date] = $visit[1];
				}
				if(!isset($_GET['s']) || !isset($_GET['e'])) {
					file_put_contents( "cache/" . Tempuserid . "/spend", json_encode( $return ) );
				}

				return $return;
			} else {
				return false;
			}
		}
	}

	function get_impressionsChart()
	{
		global $ga;
		global $db;
		if(file_exists("cache/" . Tempuserid . "/impressionsChart")) {
			if ( DebugMode == true ) {
				print "Fetching Cache";
			}
			$Cache = file_get_contents( "cache/" . Tempuserid . "/impressionsChart" );
		} else {
			fopen( "cache/" . Tempuserid . "/impressionsChart", 'w' );
			$Cache = "";
		}

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . Tempuserid . "/impressionsChart") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))) {
			return json_decode( $Cache, true );
		} else {
			print (DebugMode ? "grabbing" : "");
			if (connect()) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken($_SESSION['accessToken']);
				$id = $db->query("SELECT propertyId FROM users WHERE id=" . Tempuserid)[0]['propertyId'];
				$ga->setAccountId($id);

				if (isset($_GET['e'])) {
					if (isset($_GET['s'])) {
						if ($_GET['s'] > $_GET['e']) {
							print "	<script type=\"text/javascript\">
                                $(document).ready(function(){ 
                        
                                    demo.initChartist();
                        
                                    $.notify({
                                        icon: 'pe-7s-gift',
                                        message: \"Start Date cannot be before end date!\"
                        
                                    },{
                                        type: 'info', 
                                        timer: 4000
                                    });
                        
                                });
                            </script>";
							$_GET['s'] = date('Y-m-d', strtotime('-30 days'));
							$_GET['e'] = date('Y-m-d');
						} else {
							/* if (date('Y-m-d', strtotime('-30 days')) > $_GET['e']) {
								 print "	<script type=\"text/javascript\">
									 $(document).ready(function(){

										 demo.initChartist();

										 $.notify({
											 icon: 'pe-7s-gift',
											 message: \"Start Date cannot be before end date!\"

										 },{
											 type: 'info',
											 timer: 4000
										 });

									 });
								 </script>";
								 $_GET['s'] = strtotime('-30 days');
								 $_GET['e'] = time();
							 }*/
						}
					}
				}

				if (isset($_GET['s'])) {
					$start = date('Y-m-d', $_GET['s']);
				} else {
					$start = date('Y-m-d', strtotime('-30 days'));
				}

				if (isset($_GET['e'])) {
					$end = date('Y-m-d', $_GET['e']);
				} else {
					$end = date('Y-m-d');
				}


				$defaults = array(
					'start-date' => $start,
					'end-date' => $end,
				);
				$ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
				$params = array(
					'metrics' => 'ga:impressions',
					'dimensions' => 'ga:date',
				);
				$clicks = $ga->query($params);
				$return = [];

				foreach ($clicks['rows'] as $visit) {
					$date = $visit[0];
					$return[$date] = $visit[1];
				}
				if(!isset($_GET['s']) || !isset($_GET['e'])) {
					file_put_contents( "cache/" . Tempuserid . "/impressionsChart", json_encode( $return ) );
				}
				return $return;
			} else {
				return false;
			}
		}
	}

	function get_long_email_clicks()
	{
		global $ga;
		global $db;
		global $db;
		global $requestCache;

		if(file_exists("cache/" . Tempuserid . "/long_email_clicks")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
			$Cache = file_get_contents("cache/" . Tempuserid . "/long_email_clicks");
		} else {
			fopen("cache/" . Tempuserid . "/long_email_clicks", 'w');
			$Cache = "";
		}

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . Tempuserid . "/long_email_clicks") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))) {
			return json_decode( $Cache, true );
		} else {
			print ( DebugMode ? "grabbing" : "" );
			if ( connect() ) {
				if ( $requestCache == 0 ) {
					// Set the accessToken and Account-Id
					$ga->setAccessToken( $_SESSION['accessToken'] );
					$id = $db->query( "SELECT propertyId FROM users WHERE id=" . Tempuserid )[0]['propertyId'];
					$ga->setAccountId( $id );

					/*if (isset($_GET['s'])) {
						$start = date('Y-m-d', $_GET['s']);
					} else {
						$start = date('Y-m-d', strtotime('-1 month'));
					}

					if (isset($_GET['e'])) {
						$end = date('Y-m-d', $_GET['e']);
					} else {
						$end = date('Y-m-d');
					}*/

					$start = date( 'Y-m-d', strtotime( '-1 year' ) );
					$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


					$defaults = array(
						'start-date' => $start,
						'end-date'   => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams( $defaults );

					$params = array(
						'metrics'    => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel'
					);

					$costs = $ga->query( $params );
					$array = array();
					foreach ( $costs['rows'] as $row ) {
						if ( strpos( $row[1], "mailto:" ) !== false ) {
							$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
							if ( isset( $array[ $date ] ) ) {
								$array[ $date ] += $row[2];
							} else {
								$array[ $date ] = $row[2];
							}
						}
					}
					if ( $costs['totalResults'] > 1001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel',
							'start-index' => 1000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( strpos( $row[1], "mailto:" ) !== false ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[2];
								} else {
									$array[ $date ] = $row[2];
								}
							}
						}
					}
					if(!isset($_GET['s']) || !isset($_GET['e'])) {
						file_put_contents( "cache/" . Tempuserid . "/long_email_clicks", json_encode( $array ) );
					}
					return $array;
				}
			}
		}
	}

	function get_long_email_clicks_adwords()
	{
		global $ga;
		global $db;
		global $db;
		global $requestCache;
		if(file_exists("cache/" . Tempuserid . "/long_email_clicks_adwords")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
			$Cache = file_get_contents("cache/" . Tempuserid . "/long_email_clicks_adwords");
		} else {
			fopen("cache/" . Tempuserid . "/long_email_clicks_adwords", 'w');
			$Cache = "";
		}

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . Tempuserid . "/long_email_clicks_adwords") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))) {
			return json_decode( $Cache, true );
		} else {
			if ( connect() ) {
				if ( $requestCache == 0 ) {
					// Set the accessToken and Account-Id
					$ga->setAccessToken( $_SESSION['accessToken'] );
					$id = $db->query( "SELECT propertyId FROM users WHERE id=" . Tempuserid )[0]['propertyId'];
					$ga->setAccountId( $id );

					/*if (isset($_GET['s'])) {
						$start = date('Y-m-d', $_GET['s']);
					} else {
						$start = date('Y-m-d', strtotime('-1 month'));
					}

					if (isset($_GET['e'])) {
						$end = date('Y-m-d', $_GET['e']);
					} else {
						$end = date('Y-m-d');
					}*/

					$start = date( 'Y-m-d', strtotime( '-1 year' ) );
					$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


					$defaults = array(
						'start-date' => $start,
						'end-date'   => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams( $defaults );

					$params = array(
						'metrics'    => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel, ga:medium'
					);

					$costs = $ga->query( $params );
					$array = array();
					foreach ( $costs['rows'] as $row ) {
						if ( strpos( $row[1], "mailto:" ) !== false && $row[2] == "cpc" ) {
							$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
							if ( isset( $array[ $date ] ) ) {
								$array[ $date ] += $row[3];
							} else {
								$array[ $date ] = $row[3];
							}
						}
					}
					if ( $costs['totalResults'] > 1001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel, ga:medium',
							'start-index' => 1000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( strpos( $row[1], "mailto:" ) !== false && $row[2] == "cpc" ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[3];
								} else {
									$array[ $date ] = $row[3];
								}
							}
						}
					}
					if ( $costs['totalResults'] > 2001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel, ga:medium',
							'start-index' => 2000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( strpos( $row[1], "mailto:" ) !== false && $row[2] == "cpc" ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[3];
								} else {
									$array[ $date ] = $row[3];
								}
							}
						}
					}
					if(!isset($_GET['s']) || !isset($_GET['e'])) {
						file_put_contents( "cache/" . Tempuserid . "/long_email_clicks_adwords", json_encode( $array ) );
					}
					return $array;
				}
			}
		}
	}

	function get_long_email_clicks_organic()
	{
		global $ga;
		global $db;
		global $db;
		global $requestCache;

		if(file_exists("cache/" . Tempuserid . "/long_email_clicks_adwords")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
			$Cache = file_get_contents("cache/" . Tempuserid . "/long_email_clicks_adwords");
		} else {
			fopen("cache/" . Tempuserid . "/long_email_clicks_adwords", 'w');
			$Cache = "";
		}

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . Tempuserid . "/long_email_clicks_adwords") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))) {
			return json_decode( $Cache, true );
		} else {
			if ( connect() ) {
				if ( $requestCache == 0 ) {
					// Set the accessToken and Account-Id
					$ga->setAccessToken( $_SESSION['accessToken'] );
					$id = $db->query( "SELECT propertyId FROM users WHERE id=" . Tempuserid )[0]['propertyId'];
					$ga->setAccountId( $id );

					/*if (isset($_GET['s'])) {
						$start = date('Y-m-d', $_GET['s']);
					} else {
						$start = date('Y-m-d', strtotime('-1 month'));
					}

					if (isset($_GET['e'])) {
						$end = date('Y-m-d', $_GET['e']);
					} else {
						$end = date('Y-m-d');
					}*/

					$start = date( 'Y-m-d', strtotime( '-1 year' ) );
					$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


					$defaults = array(
						'start-date' => $start,
						'end-date'   => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams( $defaults );

					$params = array(
						'metrics'    => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel, ga:medium'
					);

					$costs = $ga->query( $params );
					$array = array();
					foreach ( $costs['rows'] as $row ) {
						if ( strpos( $row[1], "mailto:" ) !== false && $row[2] == "organic" ) {
							$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
							if ( isset( $array[ $date ] ) ) {
								$array[ $date ] += $row[3];
							} else {
								$array[ $date ] = $row[3];
							}
						}
					}
					if ( $costs['totalResults'] > 1001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel, ga:medium',
							'start-index' => 1000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( strpos( $row[1], "mailto:" ) !== false && $row[2] == "organic" ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[3];
								} else {
									$array[ $date ] = $row[3];
								}
							}
						}
					}
					if ( $costs['totalResults'] > 2001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel, ga:medium',
							'start-index' => 2000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( strpos( $row[1], "mailto:" ) !== false && $row[2] == "organic" ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[3];
								} else {
									$array[ $date ] = $row[3];
								}
							}
						}
					}
					if(!isset($_GET['s']) || !isset($_GET['e'])) {
						file_put_contents( "cache/" . Tempuserid . "/long_email_clicks_organic", json_encode( $array ) );
					}

					return $array;
				}
			}
		}
	}

function get_long_phone_clicks_adwords()
	{
		global $goalID;
		global $ga;
		global $db;
		global $db;
		global $requestCache;

		if(file_exists("cache/" . Tempuserid . "/long_phone_clicks_adwords")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
			$Cache = file_get_contents("cache/" . Tempuserid . "/long_phone_clicks_adwords");
		} else {
			fopen("cache/" . Tempuserid . "/long_phone_clicks_adwords", 'w');
			$Cache = "";
		}

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . Tempuserid . "/long_phone_clicks_adwords") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))) {
			return json_decode( $Cache, true );
		} else {
			if ( connect() ) {
				if ( $requestCache == 0 ) {
					// Set the accessToken and Account-Id
					$ga->setAccessToken( $_SESSION['accessToken'] );
					$id = $db->query( "SELECT propertyId FROM users WHERE id=" . Tempuserid )[0]['propertyId'];
					$ga->setAccountId( $id );

					/*if (isset($_GET['s'])) {
						$start = date('Y-m-d', $_GET['s']);
					} else {
						$start = date('Y-m-d', strtotime('-1 month'));
					}

					if (isset($_GET['e'])) {
						$end = date('Y-m-d', $_GET['e']);
					} else {
						$end = date('Y-m-d');
					}*/

					$start = date( 'Y-m-d', strtotime( '-1 year' ) );
					$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


					$defaults = array(
						'start-date' => $start,
						'end-date'   => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams( $defaults );

					$params = array(
						'metrics'    => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel, ga:medium'
					);

					$costs = $ga->query( $params );
					$array = array();
					foreach ( $costs['rows'] as $row ) {
						if ( strpos( $row[1], "tel:" ) !== false && $row[2] == "cpc" ) {
							$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
							if ( isset( $array[ $date ] ) ) {
								$array[ $date ] += $row[3];
							} else {
								$array[ $date ] = $row[3];
							}
						}
					}
					if ( $costs['totalResults'] > 1001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel, ga:medium',
							'start-index' => 1000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( strpos( $row[1], "tel:" ) !== false && $row[2] == "cpc" ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[3];
								} else {
									$array[ $date ] = $row[3];
								}
							}
						}
					}
					if ( $costs['totalResults'] > 2001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel, ga:medium',
							'start-index' => 2000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( strpos( $row[1], "tel:" ) !== false && $row[2] == "cpc" ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[3];
								} else {
									$array[ $date ] = $row[3];
								}
							}
						}
					}
					if(!isset($_GET['s']) || !isset($_GET['e'])) {
						file_put_contents( "cache/" . Tempuserid . "/long_phone_clicks_adwords", json_encode( $array ) );
					}

					return $array;
				}
			}
		}
	}

function get_long_phone_clicks_organic()
	{
		global $goalID;
		global $ga;
		global $db;
		global $db;
		global $requestCache;

		if(file_exists("cache/" . Tempuserid . "/long_phone_clicks_organic")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
			$Cache = file_get_contents("cache/" . Tempuserid . "/long_phone_clicks_organic");
		} else {
			fopen("cache/" . Tempuserid . "/long_phone_clicks_organic", 'w');
			$Cache = "";
		}

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . Tempuserid . "/long_phone_clicks_organic") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))) {
			return json_decode( $Cache, true );
		} else {
			if ( connect() ) {
				if ( $requestCache == 0 ) {
					// Set the accessToken and Account-Id
					$ga->setAccessToken( $_SESSION['accessToken'] );
					$id = $db->query( "SELECT propertyId FROM users WHERE id=" . Tempuserid )[0]['propertyId'];
					$ga->setAccountId( $id );

					/*if (isset($_GET['s'])) {
						$start = date('Y-m-d', $_GET['s']);
					} else {
						$start = date('Y-m-d', strtotime('-1 month'));
					}

					if (isset($_GET['e'])) {
						$end = date('Y-m-d', $_GET['e']);
					} else {
						$end = date('Y-m-d');
					}*/

					$start = date( 'Y-m-d', strtotime( '-1 year' ) );
					$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


					$defaults = array(
						'start-date' => $start,
						'end-date'   => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams( $defaults );

					$params = array(
						'metrics'    => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel, ga:medium'
					);

					$costs = $ga->query( $params );
					$array = array();
					foreach ( $costs['rows'] as $row ) {
						if ( strpos( $row[1], "tel:" ) !== false && $row[2] == "organic" ) {
							$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
							if ( isset( $array[ $date ] ) ) {
								$array[ $date ] += $row[3];
							} else {
								$array[ $date ] = $row[3];
							}
						}
					}
					if ( $costs['totalResults'] > 1001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel, ga:medium',
							'start-index' => 1000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( strpos( $row[1], "tel:" ) !== false && $row[2] == "organic" ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[3];
								} else {
									$array[ $date ] = $row[3];
								}
							}
						}
					}
					if ( $costs['totalResults'] > 2001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel, ga:medium',
							'start-index' => 2000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( strpos( $row[1], "tel:" ) !== false && $row[2] == "organic" ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[3];
								} else {
									$array[ $date ] = $row[3];
								}
							}
						}
					}
					if(!isset($_GET['s']) || !isset($_GET['e'])) {
						file_put_contents( "cache/" . Tempuserid . "/long_phone_clicks_organic", json_encode( $array ) );
					}

					return $array;
				}
			}
		}
	}

function get_long_conversions_adwords()
	{
		global $goalID;
		global $ga;
		global $db;
		global $db;
		global $requestCache;

		if(file_exists("cache/" . Tempuserid . "/long_conversions_adwords")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
			$Cache = file_get_contents("cache/" . Tempuserid . "/long_conversions_adwords");
		} else {
			fopen("cache/" . Tempuserid . "/long_conversions_adwords", 'w');
			$Cache = "";
		}

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . Tempuserid . "/long_conversions_adwords") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))) {
			return json_decode( $Cache, true );
		} else {
			if ( connect() ) {
				if ( $requestCache == 0 ) {
					// Set the accessToken and Account-Id
					$ga->setAccessToken( $_SESSION['accessToken'] );
					$id = $db->query( "SELECT propertyId FROM users WHERE id=" . Tempuserid )[0]['propertyId'];
					$ga->setAccountId( $id );

					/*if (isset($_GET['s'])) {
						$start = date('Y-m-d', $_GET['s']);
					} else {
						$start = date('Y-m-d', strtotime('-1 month'));
					}

					if (isset($_GET['e'])) {
						$end = date('Y-m-d', $_GET['e']);
					} else {
						$end = date('Y-m-d');
					}*/

					$start = date( 'Y-m-d', strtotime( '-1 year' ) );
					$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


					$defaults = array(
						'start-date' => $start,
						'end-date'   => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams( $defaults );

					$params = array(
						'metrics'    => 'ga:goal' . $goalID . 'Completions',
						'dimensions' => 'ga:yearmonth, ga:medium'
					);

					$costs = $ga->query( $params );
					$array = array();
					foreach ( $costs['rows'] as $row ) {
						if ( $row[1] == "cpc" ) {
							$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
							if ( isset( $array[ $date ] ) ) {
								$array[ $date ] += $row[2];
							} else {
								$array[ $date ] = $row[2];
							}
						}
					}

					if ( $costs['totalResults'] > 1001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel, ga:medium',
							'start-index' => 1000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( $row[1] == "cpc" ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[2];
								} else {
									$array[ $date ] = $row[2];
								}
							}
						}
					}
					if ( $costs['totalResults'] > 2001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel, ga:medium',
							'start-index' => 2000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( $row[1] == "cpc" ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[2];
								} else {
									$array[ $date ] = $row[2];
								}
							}
						}
					}
					if(!isset($_GET['s']) || !isset($_GET['e'])) {
						file_put_contents( "cache/" . Tempuserid . "/long_conversions_adwords", json_encode( $array ) );
					}

					return $array;
				}
			}
		}
	}

	function get_long_conversions_organic()
	{
		global $goalID;
		global $ga;
		global $db;
		global $db;
		global $requestCache;

		if(file_exists("cache/" . Tempuserid . "/long_conversions_organic")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
			$Cache = file_get_contents("cache/" . Tempuserid . "/long_conversions_organic");
		} else {
			fopen("cache/" . Tempuserid . "/long_conversions_organic", 'w');
			$Cache = "";
		}

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . Tempuserid . "/long_conversions_organic") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))) {
			return json_decode( $Cache, true );
		} else {
			if ( connect() ) {
				if ( $requestCache == 0 ) {
					// Set the accessToken and Account-Id
					$ga->setAccessToken( $_SESSION['accessToken'] );
					$id = $db->query( "SELECT propertyId FROM users WHERE id=" . Tempuserid )[0]['propertyId'];
					$ga->setAccountId( $id );

					/*if (isset($_GET['s'])) {
						$start = date('Y-m-d', $_GET['s']);
					} else {
						$start = date('Y-m-d', strtotime('-1 month'));
					}

					if (isset($_GET['e'])) {
						$end = date('Y-m-d', $_GET['e']);
					} else {
						$end = date('Y-m-d');
					}*/

					$start = date( 'Y-m-d', strtotime( '-1 year' ) );
					$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


					$defaults = array(
						'start-date' => $start,
						'end-date'   => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams( $defaults );

					$params = array(
						'metrics'    => 'ga:goal' . $goalID . 'Completions',
						'dimensions' => 'ga:yearmonth, ga:medium'
					);

					$costs = $ga->query( $params );
					$array = array();
					foreach ( $costs['rows'] as $row ) {
						if ( $row[1] == "organic" ) {
							$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
							if ( isset( $array[ $date ] ) ) {
								$array[ $date ] += $row[2];
							} else {
								$array[ $date ] = $row[2];
							}
						}
					}

					if ( $costs['totalResults'] > 1001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel, ga:medium',
							'start-index' => 1000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( $row[1] == "organic" ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[2];
								} else {
									$array[ $date ] = $row[2];
								}
							}
						}
					}
					if ( $costs['totalResults'] > 2001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel, ga:medium',
							'start-index' => 2000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( $row[1] == "organic" ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[2];
								} else {
									$array[ $date ] = $row[2];
								}
							}
						}
					}
					if(!isset($_GET['s']) || !isset($_GET['e'])) {
						file_put_contents( "cache/" . Tempuserid . "/long_conversions_organic", json_encode( $array ) );
					}
					return $array;
				}
			}
		}
	}


	function get_long_phone_clicks()
	{
		global $ga;
		global $db;
		global $db;
		global $requestCache;

		if(file_exists("cache/" . Tempuserid . "/long_phone_clicks")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	}
			$Cache = file_get_contents("cache/" . Tempuserid . "/long_phone_clicks");
		} else {
			fopen("cache/" . Tempuserid . "/long_phone_clicks", 'w');
			$Cache = "";
		}

		if($Cache != "" && DebugMode != true &&filemtime("cache/" . Tempuserid . "/long_phone_clicks") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))) {
			return json_decode( $Cache, true );
		} else {
			if ( connect() ) {
				if ( $requestCache == 0 ) {
					// Set the accessToken and Account-Id
					$ga->setAccessToken( $_SESSION['accessToken'] );
					$id = $db->query( "SELECT propertyId FROM users WHERE id=" . Tempuserid )[0]['propertyId'];
					$ga->setAccountId( $id );

					/*if (isset($_GET['s'])) {
						$start = date('Y-m-d', $_GET['s']);
					} else {
						$start = date('Y-m-d', strtotime('-1 month'));
					}

					if (isset($_GET['e'])) {
						$end = date('Y-m-d', $_GET['e']);
					} else {
						$end = date('Y-m-d');
					}*/

					$start = date( 'Y-m-d', strtotime( '-1 year' ) );
					$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


					$defaults = array(
						'start-date' => $start,
						'end-date'   => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams( $defaults );

					$params = array(
						'metrics'    => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel'
					);

					$costs = $ga->query( $params );
					$array = array();
					foreach ( $costs['rows'] as $row ) {
						if ( strpos( $row[1], "tel:" ) !== false ) {
							$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
							if ( isset( $array[ $date ] ) ) {
								$array[ $date ] += $row[2];
							} else {
								$array[ $date ] = $row[2];
							}
						}
					}
					if ( $costs['totalResults'] > 1001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel',
							'start-index' => 1000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( strpos( $row[1], "tel:" ) !== false ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[2];
								} else {
									$array[ $date ] = $row[2];
								}
							}
						}
					}
					if ( $costs['totalResults'] > 2001 ) {
						$start = date( 'Y-m-d', strtotime( '-1 year' ) );
						$end   = date( 'Y-m-d', strtotime( 'last day of previous month' ) );


						$defaults = array(
							'start-date' => $start,
							'end-date'   => $end,
						);

						/* COSTS */

						$ga->setDefaultQueryParams( $defaults );

						$params = array(
							'metrics'     => 'ga:totalEvents',
							'dimensions'  => 'ga:yearmonth, ga:eventLabel',
							'start-index' => 2000
						);

						$costs = $ga->query( $params );
						foreach ( $costs['rows'] as $row ) {
							if ( strpos( $row[1], "tel:" ) !== false ) {
								$date = substr( $row[0], 0, 4 ) . substr( $row[0], 4, 2 );
								if ( isset( $array[ $date ] ) ) {
									$array[ $date ] += $row[2];
								} else {
									$array[ $date ] = $row[2];
								}
							}
						}
					}
					if(!isset($_GET['s']) || !isset($_GET['e'])) {
						file_put_contents( "cache/" . Tempuserid . "/long_phone_clicks", json_encode( $array ) );
					}
					return $array;
				}
			}
		}
	}

	function get_allConversionsChart()
	{
		$emails = get_long_email_clicks();
		$phone = get_long_phone_clicks();
		$forms = array_reverse(get_long_conversions(), true);

		$array = "";
		foreach ($forms as $date => $formsCount) {
			$thedate = substr($date, 2, 2) . "/" . substr($date, 4, 2);
			if(isset($phone[$date])){
				$phoneClicks = $phone[$date];
			} else {
				$phoneClicks = 0;
			}
			if(isset($emails[$date])){
				$emailClicks = $emails[$date];
			} else {
				$emailClicks = 0;
			}
			if($array == ""){
				$array .= "['" . $thedate . "', " . $formsCount . ", " . $emailClicks . ", " . $phoneClicks . "]";
			} else {
				$array .= ",['" . $thedate . "', " . $formsCount . ", " . $emailClicks . ", " . $phoneClicks . "]";
			}
		}

		return $array;

	}


	function get_allConversionsChartAdwords()
	{
		$emails = get_long_email_clicks_adwords();
		$phone = get_long_phone_clicks_adwords();
		$forms = get_long_conversions_adwords();

		$array = "";
		foreach ($forms as $date => $formsCount) {
			$thedate = substr($date, 0, 4) . "/" . substr($date, 4, 2);
			if(isset($phone[$date])){
				$phoneClicks = $phone[$date];
			} else {
				$phoneClicks = 0;
			}
			if(isset($emails[$date])){
				$emailClicks = $emails[$date];
			} else {
				$emailClicks = 0;
			}
			if($array == ""){
				$array .= "['" . $thedate . "', " . $formsCount . ", " . $emailClicks . ", " . $phoneClicks . "]";
			} else {
				$array .= ",['" . $thedate . "', " . $formsCount . ", " . $emailClicks . ", " . $phoneClicks . "]";
			}
		}

		return $array;

	}

	function get_allConversionsChartOrganic()
	{
		$emails = get_long_email_clicks_organic();
		$phone = get_long_phone_clicks_organic();
		$forms = get_long_conversions_organic();

		$array = "";
		foreach ($forms as $date => $formsCount) {
			$thedate = substr($date, 0, 4) . "/" . substr($date, 4, 2);
			if(isset($phone[$date])){
				$phoneClicks = $phone[$date];
			} else {
				$phoneClicks = 0;
			}
			if(isset($emails[$date])){
				$emailClicks = $emails[$date];
			} else {
				$emailClicks = 0;
			}
			if($array == ""){
				$array .= "['" . $thedate . "', " . $formsCount . ", " . $emailClicks . ", " . $phoneClicks . "]";
			} else {
				$array .= ",['" . $thedate . "', " . $formsCount . ", " . $emailClicks . ", " . $phoneClicks . "]";
			}
		}

		return $array;

	}


	function get_long_conversions() {
	global $conversionsURL;
		$conversions = json_decode(file_get_contents($conversionsURL . "?type=month&count=1000"), true);
		$array = array();
		foreach ($conversions as $year => $months){
			foreach ($months as $month => $count){
				$date = "20" . $year . $month;
				$array[$date] = $count;
			}
		}
		return $array;
	}

	function get_conversions() {
		global $conversionsURL;
		$conversions = json_decode(file_get_contents($conversionsURL . "?type=json&count=1000"), true);


		return $conversions;
	}

function  get_Percent_conversion_rates_average(){
	global $conversionsURL;
	global $goalID;
    global $ga;
    global $db;
    if(file_exists("cache/" . Tempuserid . "/percentConvRatesavg")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	} 
 $Cache = file_get_contents("cache/" . Tempuserid . "/percentConvRatesavg"); 
 } else { 
 //fopen("cache/" . Tempuserid . "/percentConvRatesavg" 'w'); 
 $Cache = ""; 
 }

    if ($Cache != "" && filemtime("cache/" . Tempuserid . "/percentConvRatesavg") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))) {
        return $Cache;
    } else {
        print (DebugMode ? "grabbing" : "");
        if (connect()) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . Tempuserid)[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-30 days'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            $ga->setDefaultQueryParams($defaults);


            /* CONVERSIONS */

            $params = array(
                'metrics' => 'ga:goal' . $goalID . 'ConversionRate'
            );

            $goals = $ga->query($params);
            $conversions = $goals['totalsForAllResults']['ga:goal' . $goalID . 'ConversionRate'];
            if(!isset($_GET['s']) || !isset($_GET['e'])) {
	            file_put_contents( "cache/" . Tempuserid . "/percentConvRatesavg", $conversions );
            }
            return $conversions;

        }
    }
}

function  get_keywords() {
	global $conversionsURL;
	global $goalID;
    global $ga;
    global $db;

    if(file_exists("cache/" . Tempuserid . "/keywords")) { 
	if ( DebugMode == true ) {
		print "Fetching Cache";
	} 
 $Cache = file_get_contents("cache/" . Tempuserid . "/keywords"); 
 } else { 
 //fopen("cache/" . Tempuserid . "/keywords" 'w'); 
 $Cache = ""; 
 }

    if($Cache != "" && DebugMode != true &&filemtime("cache/" . Tempuserid . "/keywords") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
        return json_decode($Cache, true);
    } else {
        print (DebugMode ? "grabbing" : "");
        if (connect()) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . Tempuserid)[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                //$start = date('Y-m-d', strtotime('-30 days'));
                $start = date('Y-m-d', strtotime('29th november 2017'));
                //var_dump($start);
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            $ga->setDefaultQueryParams($defaults);


            /* CONVERSIONS */

            $params = array(
                'metrics' => 'ga:adCost,ga:goal' . $goalID . 'Completions,ga:sessions',
                'dimensions' => 'ga:keyword'

            );
            $keywords = array();
            $keywordCost = $ga->query($params);

            foreach ($keywordCost['rows'] as $row) {
                $keyword[$row[0]]["cost"] = $row[1];
                $keyword[$row[0]]["conversions"] = $row[2];
                $keyword[$row[0]]["users"] = $row[3];
                if ($keyword[$row[0]]["conversions"] != 0 && $keyword[$row[0]]["users"] != 0) {
                    $keyword[$row[0]]["conversionRate"] = $keyword[$row[0]]["conversions"] / $keyword[$row[0]]["users"];
                } else {
                    $keyword[$row[0]]["conversionRate"] = 0;
                }

                if ($keyword[$row[0]]["cost"] != 0 && $keyword[$row[0]]["conversions"] != 0) {
                    $keyword[$row[0]]["costPerConversion"] = $keyword[$row[0]]["cost"] / $keyword[$row[0]]["conversions"];
                } else {
                    $keyword[$row[0]]["costPerConversion"] = $keyword[$row[0]]["cost"];
                }
            }
            if(!isset($_GET['s']) || !isset($_GET['e'])) {
	            file_put_contents( "cache/" . Tempuserid . "/keywords", json_encode( $keyword ) );
            }
            return $keyword;

        } else {
            return false;
        }
    }
}
