<?php
    ini_set('display_errors', 1);
    include_once ('functions.php');
	$siteurl = "http://support.matm.co.uk/riwal";
    ?>
<html>
<head>
    <link rel="stylesheet" href="riwal.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function () {
            $('table:not(.noColor)').each(function () {
                var count = 0;
                $(this).find('tr').each(function () {
                    if (count == 0) {
                        count = 1;
                        $(this).addClass('odd')
                    } else {
                        count = 0;
                        $(this).addClass('even')
                    }
                })
            });
        });
    </script>
    <style>
        .card {
            padding: 10px;
        }

        table.keystats, table.keystats tbody {
            width: 100%;
            height: 50px;
            display: block;
            margin: auto;
        }

        .visibility td, .domain_score td, .traffic td, .historic_traffic td, .glossary td, .look_forward td, .social_media td, .site td {
            padding-left: 60px;
        }


        .domain_stats_data td img {
            width:  25px;
            display:  inline-block;
        }

        .keystats tr {
            width:  100%;
            display: block;
            padding-bottom: 20px;
        }

        .keystats tr td {
            width: 19%;
            display: inline-block;
            text-align: center;
        }

        .keystats tr td p {
            text-align:  center;
            display: inline-block;
        }

        .key_stats_holder {
            min-height: 100px;
        }

        .key_stats_holder .loading {
            top: 45px;
        }

        body hr {
            border-top: 3px solid #beb5a8;
        }
        @media print {
            tr.vendorListHeading {
                background-color: #1a4567 !important;
                -webkit-print-color-adjust: exact;
            }
        }

        @media print {
            .vendorListHeading th {
                color: white !important;
            }
        }
        h2.section_title {
            color: #242424 !important;
            margin-top: 50px;
            background: #ffc20e;
            -webkit-print-color-adjust: exact;
            padding: 10px;
            padding-left: 60px;
            font-size: 24px;
            font-family: arial;
            font-style: italic;
        }

        .description {
            padding: 0  10px;
        }

        table.padded, table.smaller{
            width:  100%;
        }

        table.padded tr, table.smaller tr, table.coloured tr {
            border-bottom: 1px solid #e0dcd7;
        }

        table.padded tr:nth-child(1), table.smaller tr:nth-child(1), table.coloured tr:nth-child(1) {
            border-top: 1px solid #e0dcd7;
        }

        table.padded td {
            padding: 10px 10px;
        }

        table.smaller td {
            padding:  0 10px;
        }

        table.coloured {
            width:  100%;
        }

        table.coloured tr {
            height:  40px;
        }

        table.coloured tr {
            text-align: center;
        }

        table.coloured tr td {
            width:  20%;
        }

        table.coloured tr.answers td.true {
            background: green;
        }

        table.coloured tr.answers td.false {
            background: red;
        }

        tr.odd {}

        tr.even {
            background: #FFF7DE;
        }
    </style>
</head>

<body>
<div class=" paper">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="card">
                        <h1 style="text-align: center;">SEO Review - Riwal</h1>
                        <p style="text-align: center"><strong>April 2018</strong>.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2 class="section_title">Key Stats</h2>
                </div>
            </div>
            <div class="row">
                <div class="card">
                    <div class="content">
                        <table width="" border="0" class="keystats">
                            <tr class="domain_stats_data odd">
                                <td id="page_sessions">
                                    <img src="http://support.matm.co.uk/riwal/img/chain-link.png" />
                                    <p>Inbound Links:
                                        <br/> <strong class="sessions saving">41,402</strong>
                                    </p>
                                </td>
                                <td id="users">
                                    <img src="http://support.matm.co.uk/riwal/img/chain-link.png" />
                                    <p>Inbound Link Domains:
                                        <br/> <strong>569</strong>
                                    </p>
                                </td>
                                <td id="new_unique_users">
                                    <img src="http://support.matm.co.uk/riwal/img/visibility.png" />
                                    <p>Keywords Tracked:
                                        <br/> <strong>56</strong>
                                    </p>
                                </td>
                                <td id="pages_viewed">
                                    <img src="http://support.matm.co.uk/riwal/img/visibility.png" />
                                    <p>Keywords in top 10:
                                        <br/> <strong>2</strong>
                                    </p>
                                </td>
                                <td id="avg_pages_viewed">
                                    <img src="http://support.matm.co.uk/riwal/img/visibility.png" />
                                    <p>Keywords in top 100:
                                        <br/> <strong>14</strong>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2 class="section_title">Local Listings</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="card">
                        <div class="content">
                            <table class="coloured noColor">
                                <tr class="even">
                                    <td></td>
                                    <td>Google Maps</td>
                                    <td>Google Business</td>
                                    <td>Bing Places</td>
                                    <td>Yell</td>
                                </tr>
                                <tr class="answers ">
                                    <td>Listed?</td>
                                    <td class="true">&#10004;</td>
                                    <td class="true">&#10004;</td>
                                    <td class="true">&#10004;</td>
                                    <td class="true">&#10004;</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2 class="section_title">Rank Analysis</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="description">
                            <h3>What are keyword positions?</h3>
                            <p>Keyword position refers to the position your website shows up in a google search for a specific keyword. The higher the Rank, the more visible you are when someone is searching, which in turn gives you the potential for more users on the website.</p>
                            <h3>Areas for improvement</h3>
                            <p>Currently, out of the 56 Keywords we are tracking, only 2 of them are in the top 10, and only 14 are in the top 100.</p>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-6">
                                <table class="padded">
                                    <tr class="">
                                        <td>Total Keywords Tracked:</td>
                                        <td>
                                            <p><strong>56</strong>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr class="">
                                        <td>Keywords in top 10:</td>
                                        <td>
                                            <p><strong>2</strong> (3.57%)</p>
                                        </td>
                                    </tr>
                                    <tr class="">
                                        <td>Keywords in top 100:</td>
                                        <td>
                                            <p><strong>14</strong> (25%)</p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="smaller">
                                    <p><strong>Top 5 Keywords</strong>.</p>
                                    <tr class="">
                                        <th style="text-align: center"><strong>Keyword</strong>
                                        </td>
                                        <th><strong>Position</strong>
                                        </th>
                                    </tr>
                                    <tr class="">
                                        <td>aerial work platform hire</td>
                                        <td><strong>7</strong>
                                        </td>
                                    </tr>
                                    <tr class="">
                                        <td>aerial work platform</td>
                                        <td><strong>10</strong>
                                        </td>
                                    </tr>
                                    <tr class="">
                                        <td>aerial work platforms</td>
                                        <td><strong>12</strong>
                                        </td>
                                    </tr>
                                    <tr class="">
                                        <td>ultra booms</td>
                                        <td><strong>13</strong>
                                        </td>
                                    </tr>
                                    <tr class="">
                                        <td>Access hire</td>
                                        <td><strong>54</strong>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <br/>
                        <div class="description">
                            <h3>MATM Proposal</h3>
                            <p>Google rankings can be improved through On page and Off page SEO. Regular content (news stories) and technical assistance (below) will help. See separate keyword rankings report.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2 class="section_title">Directories and External Links</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="description">
                            <h3>What are Directories and External Links</h3>
                            <p>External links are important for SEO, as they show search engines that your site is trusted and respected enough by other websites for them to include a link to your website.</p>
                            <p>Although, an important thing to note is it's not all about quantity. Search engines prefer inbound links from other large, trusted websites as apposed to lots of spam links from smaller, less trusted websites.</p>
                            <h3>Areas for improvement</h3>
                            <p>The Riwal website is very strong in this area. Having a large amount of good quality back links, form domains with a high domain score. However, it is important to keep a flow a fresh Back Links to your site.</p>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-6">
                                <table class="padded">
                                    <tr class="">
                                        <td>Total Inbound Links:</td>
                                        <td>
                                            <p><strong>41,402</strong>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr class="">
                                        <td>Links from unique domains:</td>
                                        <td>
                                            <p><strong>569</strong>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Link Spam Score</td>
                                        <td>
                                            <p><strong>2%</strong>
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="smaller">
                                    <p><strong>Top 5 Linking Domains</strong>.</p>
                                    <tr>
                                        <td style="text-align: center"><strong>Domain</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">de.wikipedia.org</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">home.pl</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">mirror.co.uk</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">ask.com</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">viadeo.com</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <br/>
                        <div class="description">
                            <h3>MATM Proposal</h3>
                            <p>Going forward, it is important to keep gaining fresh back links to your site from trusted sources. We would do this by Adding the business and website to online directories every month.</p>
                            <p>These Directories will include sites such as:.</p>
                            <ul>
                                <li><a target="_blank" href="https://www.yell.com/">yell.co.uk</a>
                                </li>
                                <li><a target="_blank" href="http://ukbusinessdirectoryltd.co.uk">ukbusinessdirectoryltd.co.uk</a>
                                </li>
                                <li><a target="_blank" href="http://192.com">192.com</a>
                                </li>
                            </ul>
                            <p>Doing this will keep a steady flow of back links coming in to the site, showing search engines that the site is still relevant and trusted.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2 class="section_title">Page Meta Titles</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="description">
                            <h3>What are Page Meta Titles?</h3>
                            <p>Meta Titles are what search engines use for the link text in a search result. If a meta title is not set, the search engine will use other data, such as headings or text in the page to set the title.</p>
                            <p>Because of this, it is important to set titles for all pages, to ensure the search engine results a user sees are accurate and encourage them to click through to your site.</p>
                            <p>It is also important to keep each page title unique, as search engines punish you for having too many duplicate titles on your site.</p>
                            <h3>Areas for improvement</h3>
                            <p>On the Riwal site, there is an issue with duplicate Titles. A third of all the titles on the site are not unique, this is picked up by search engines and your rankings will be penalised because of it.</p>
                            <p>Also, as well as the duplicates, most of the page titles are bellow the recommended size, and therefor are likely not as descriptive as they could be. This means that Riwal may not appear in some searches that they otherwise would, as the titles are not optimised for those keywords.</p>
                        </div>
                        <br/>
                        <div class="pagebreak"></div>

                        <div class="row">
                            <div class="col-md-6">
                                <table class="padded">
                                    <tr>
                                        <td>Meta Titles missing:</td>
                                        <td>
                                            <p><strong>0</strong> (0%)</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Duplicate titles:</td>
                                        <td>
                                            <p><strong>26</strong> (33.33%)</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Titles too long (Over 65 characters):</td>
                                        <td>
                                            <p><strong>3</strong> (3.85%)</p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="padded">
                                    <tr>
                                        <td>Titles too short (under 30 characters):</td>
                                        <td>
                                            <p><strong>64</strong> (82.05%)</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Titles too long (over 568 pixels):</td>
                                        <td>
                                            <p><strong>2</strong> (2.56%)</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Titles too short (under 200 pixels):</td>
                                        <td>
                                            <p><strong>62</strong> (79.49%)</p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <br/>
                        <div class="description">
                            <h3>MATM Proposal</h3>
                            <p>To fix this issue, we would go through each of the pages on the site that don't have a unique title, and edit the title to something more optimised for search, whilst still remaining relevant to the content.</p>
                            <p>We would also tackle the titles that are too long. There are multiple things we can do to these titles to make sure they are as optimised as possible. This ranges from changing a few words in the titles that are only slightly too long, to changing the title completely on the pages where the title is a lot over the limit.</p>
                            <p>We would then do the same with the titles that are too short, making them much more specific to the page so that they can be easily found in search engine results.</p>
                        </div>
                        <div class="float-right time">
                            <h4>Time: 2-5 Hours</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2 class="section_title">Page Meta Descriptions</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="description">
                            <h3>What are Page Meta Descriptions?</h3>
                            <p>Similar to Meta Titles, Meta Descriptions are used by search engines to show a description of your website in search results.</p>
                            <p>It is also important to set a unique description for every page on your website. These descriptions should be a maximum of 156 characters long (or 940px) so they aren't truncated, and a minimum of around 70 characters long (or 400px).</p>
                            <h3>Areas for improvement</h3>
                            <p>On the Riwal site, there are lots of pages without a description tag. This means that when your page shows up in a search results page, the description for it is automatically generated from the page content. This means you miss out on an opportunity to attract users to your website by providing a relevant description.</p>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-6">
                                <table class="padded">
                                    <tr>
                                        <td>Meta Descriptions missing:</td>
                                        <td>
                                            <p><strong>52</strong> (66.67%)</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Duplicate Meta Descriptions:</td>
                                        <td>
                                            <p><strong>4</strong> (5.13%)</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Descriptions too long (Over 156 characters):</td>
                                        <td>
                                            <p><strong>13</strong> (16.67%)</p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="padded">
                                    <tr>
                                        <td>Descriptions too short (under 70 characters):</td>
                                        <td>
                                            <p><strong>55</strong> (70.51%)</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Descriptions too long (over 940 pixels):</td>
                                        <td>
                                            <p><strong>8</strong> (10.26%)</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Descriptions too short (under 400 pixels):</td>
                                        <td>
                                            <p><strong>55</strong> (70.51%)</p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <br/>
                        <div class="description">
                            <h3>MATM Proposal</h3>
                            <p>We would start with the pages where the meta descriptions are missing, crafting a description of each individual page that is relevant to the content, but at the same time optimised for search engines and enticing for users.</p>
                            <p>Once we had filled all of the missing descriptions, we could then focus on editing the duplicate descriptions along with those that are outside the size limit. We would tweak these to make them both individual and unique to the page, and fit within the recommended minimum and maximum sizes.</p>
                        </div>
                        <div class="float-right time">
                            <h4>Time: 4-8 Hours</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2 class="section_title">Page Headings</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="description">
                            <h3>What are Page Headings?</h3>
                            <p>Page headings allow search engines to get a picture of what the page is about, so it can be clarified for search results. The more accurate the headings are, the easier it is for the search engine to understand what the page is about.</p>
                            <h3>Areas for improvement</h3>
                            <p>There are some pages that have no headings, this means that when a search engine crawls the site, it's harder for them to understand what is on the page and what the page is about. This will be reflected in your keyword rankings, as you won't rank as high if the search engine can't understand what the page is.</p>
                            <p>There are also some duplicate headings on the site. This means that the site could be penalised in the keyword rankings for duplicate content.</p>
                            <p>Also, whilst not as important as the main heading, sub-headings are an important part here that could be causing the site to rank lower in searches.</p>
                            <p>There are a lot of pages where there are no sub-headings and, although not relvent on every page, many of them could benefit from a descriptive sub-heading in the content.</p>
                        </div>
                        <div class="row">
                            <br/>
                            <div class="col-md-6">
                                <p style="text-align: center"><strong>Headings (H1)</strong>.</p>
                                <table class="padded">
                                    <tr>
                                        <td>Page Headings Missing:</td>
                                        <td>
                                            <p><strong>18</strong> (23.08%)</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Duplicate Page Headings:</td>
                                        <td>
                                            <p><strong>11</strong> (14.10%)</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Page Headings Over 70 Characters:</td>
                                        <td>
                                            <p><strong>3</strong> (3.85%)</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Multiple Page Headings on page:</td>
                                        <td>
                                            <p><strong>8</strong> (10.26%)</p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <p style="text-align: center"><strong>Sub-Headings (H2)</strong>.</p>
                                <table class="padded">
                                    <tr>
                                        <td>Sub-Headings Missing:</td>
                                        <td>
                                            <p><strong>56</strong> (71.79%)</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Duplicate Sub-Headings:</td>
                                        <td>
                                            <p><strong>7</strong> (8.97%)</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sub-Headings Over 70 Characters:</td>
                                        <td>
                                            <p><strong>0</strong> (0.0%)</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Multiple Sub-Headings on page:</td>
                                        <td>
                                            <p><strong>19</strong> (24.36%)</p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <br/>
                        <div class="description">
                            <h3>MATM Proposal</h3>
                            <p>We would rectify the pages that are missing a heading, by adding a relevant one to the page that fits with the content.</p>
                            <p>After sorting that issue, we would then move on to the duplicates, changing them to better reflect the content on each page where possible.</p>
                            <p>Once the headings are sorted, we could then focus on sub-headings, adding in missing sub-headings where appropriate, relevant to the content.</p>
                        </div>
                        <div class="float-right time">
                            <h4>Time: 4-8 Hours</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pagebreak"></div>

            <div class="row">
                <div class="col-md-12">
                    <h2 class="section_title">Images</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="description">
                            <h3>What are images?</h3>
                            <p>Having images set up correctly on your website is essential for SEO. Search engines can't see what the image is, so they rely on the image meta data, such as "alt" tags and "title" tags.</p>
                            <p>It is also important to optimise image sizes so they don't slow the sites load times dramatically. You should aim to have images under 100kb in size where possible.</p>
                            <h3>Areas for improvement</h3>
                            <p>There are some images on the Riwal site that are over 100kb. This means that when they are present on a page, that page will load noticeably slower. This is important for both SEO and user experience.</p>
                            <p>Google takes into account page loading times when ranking your site for search results, so if there are large images slowing down the site, your rankings will suffer.</p>
                            <p>The other aspect to take into account is the user experience. Studies have shown that users can start to abandon a site after as little as 3 seconds of loading, so the longer your site takes to load, the less users there are who will wait.</p>
                            <p>Another issue is the missing Alt tags. Google uses 'bots' to crawl a website and work out what is on it, and so they can't necessarily accurately tell what an image is, and without the images having alt tags, the images may be misclassified or ignored by the search engines.</p>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-6">
                                <table class="padded">
                                    <tr>
                                        <td>Images Over 100 kb:</td>
                                        <td>
                                            <p><strong>33</strong> (39.29%)</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Images with Missing Alt Text:</td>
                                        <td>
                                            <p><strong>70</strong> (83.33%)</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Images Alt Text Over 100 Characters:</td>
                                        <td>
                                            <p><strong>2</strong> (2.38%)</p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="smaller">
                                    <p><strong>Top 5 largest images</strong>.</p>
                                    <tr>
                                        <td>Aerospace-600x300.ashx</td>
                                        <td>2114 KB</td>
                                    </tr>
                                    <tr>
                                        <td>JLG-600AJ---375x260.ashx</td>
                                        <td>756 KB</td>
                                    </tr>
                                    <tr>
                                        <td>TrackTrace2.ashx</td>
                                        <td>687 KB</td>
                                    </tr>
                                    <tr>
                                        <td>Construction-600x300.ashx</td>
                                        <td>290 KB</td>
                                    </tr>
                                    <tr>
                                        <td>Film-and-TV-600x300-Mk2.ashx</td>
                                        <td>248 KB</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <br/>
                        <div class="description">
                            <h3>MATM Proposal</h3>
                            <p>We would start by optimizing all of the images on the site. This would be done using a lossless optimisation algorithm, meaning that the images won't have any visual differences when optimised, but the file size would be drastically reduced, and in turn so would the page loading times.</p>
                            <p>We also work on adding accurate, relevant and SEO optimised alt tags to all of the images on the site, letting search engines see what the image is, and therefore helping them better classify the site for optimised keyword rankings.</p>
                        </div>
                        <div class="float-right time">
                            <h4>Time: 1-3 Hours</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

</html>