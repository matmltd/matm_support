<?php

$servername = "localhost";
$username = "matm_support";
$password = "studio32";
$dbname = "matm_support";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$clientid = $conn->real_escape_string($_POST['clientID']);
$message = $conn->real_escape_string($_POST['message']);
$email = $conn->real_escape_string($_POST['ccemail']);
$subject = $conn->real_escape_string($_POST['type'] . " - " . $_POST['subject']);

$sql = "INSERT INTO tickets (clientId, ticketBody, subject, ccemail)
VALUES ($clientid, '$message', '$subject', '$email')";

$clientsql = "SELECT * FROM clients WHERE id=" . $conn->real_escape_string($_POST['clientID']) . " LIMIT 1";

$client = $conn->query($clientsql)->fetch_assoc();

if ($conn->query($sql) === TRUE) {
	$id = $conn->insert_id;
	$headers[] = 'MIME-Version: 1.0';
	$headers[] = 'Content-type: text/html; charset=iso-8859-1';
	
	$body = "
	<html>
		<head>
		
		</head>
		<body>
			<h2>New Support Ticket</h2>
			<p>Subject: " . $subject . " </p>
			<p>Body:" . str_replace("\r\n", "<br/>", $message) . "</p>	
			<p>Email: " . $email . "</p>
			<p>ClientID: " . $clientid . " - " . $client['name'] . "</p>
			<p>ID: " . $id . "</p>
		</body>
	</html>";
	$to = "jtc@matm.co.uk";
	$subject = "New Support Ticket";
	
	mail($to, $subject, $body, implode("\r\n", $headers));
	$conn->close();

	header("Location: " . $_POST['returnURL'] . "?ticketid=" . $id);
} else {
	$errorcode = rand(100, 999);
	mail("jtc@matm.co.uk", "Error with support ticket", "Error When submiting a support ticket. Error Code: " . $errorcode . "\r\n" .  $sql . "\r\n" . $conn->error);
    echo "<h2> OOPS! </h2> <p> There has been an Error submitting your support ticket. Please get in touch with the MATM web team at web@matm.co.uk, quoting the error code: " . $errorcode . "</p>";
}
