<?php

$servername = "localhost";
$username = "matm_support";
$password = "studio32";
$dbname = "matm_support";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$secret = $conn->real_escape_string(urldecode($_GET['clientsecret']));
$clientid = $conn->real_escape_string($_GET['clientid']);

$sql = "SELECT * FROM clients WHERE id=$clientid LIMIT 1";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $client = $row;
    }
} else {
	print "{}";
	exit;
}

if($client['secret'] === $secret){
	// SECURED 
	if(!isset($_GET['ticketid'])){

		$result = $conn->query("SELECT * FROM tickets WHERE clientID=$clientid AND closed=0");

		if ($result->num_rows > 0) {
    		// output data of each row
		    while($row = $result->fetch_assoc()) {
    	    	$tickets[] = $row;
    		}
    	
			print json_encode($tickets);
		} else {
			print "{}";
			exit;
		}
	} else {
		$ticketid = $_GET['ticketid'];
		$result = $conn->query("SELECT * FROM tickets WHERE clientID=$clientid AND id=$ticketid");

		if ($result->num_rows > 0) {
    		// output data of each row
		    while($row = $result->fetch_assoc()) {
    	    	$tickets[] = $row;
    		}
    	
			print json_encode($tickets);
		} else {
			print "{}";
			exit;
		}
	
	}
} else {
	print "unauthorised";
	exit(503);
}